--Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2016.3 (win64) Build 1682563 Mon Oct 10 19:07:27 MDT 2016
--Date        : Wed Aug 02 17:12:25 2017
--Host        : BioPerzept-PC running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target zed_design_wrapper.bd
--Design      : zed_design_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity zed_design_wrapper is
  port (
    CH_A_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    CH_A_clk : out STD_LOGIC;
    CH_A_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_A_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_A_en : out STD_LOGIC;
    CH_A_rst : out STD_LOGIC;
    CH_A_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CH_B_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    CH_B_clk : out STD_LOGIC;
    CH_B_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_B_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_B_en : out STD_LOGIC;
    CH_B_rst : out STD_LOGIC;
    CH_B_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FFT_IM_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    FFT_IM_clk : out STD_LOGIC;
    FFT_IM_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_IM_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_IM_en : out STD_LOGIC;
    FFT_IM_rst : out STD_LOGIC;
    FFT_IM_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    FFT_RE_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    FFT_RE_clk : out STD_LOGIC;
    FFT_RE_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_RE_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_RE_en : out STD_LOGIC;
    FFT_RE_rst : out STD_LOGIC;
    FFT_RE_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    pl_ps_0_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    pl_ps_1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ps_pl_0_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ps_pl_1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ps_pl_2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ps_pl_3_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end zed_design_wrapper;

architecture STRUCTURE of zed_design_wrapper is
  component zed_design is
  port (
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    PS_PL_0_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PS_PL_1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PL_PS_0_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    PL_PS_1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_A_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    CH_A_clk : out STD_LOGIC;
    CH_A_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_A_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_A_en : out STD_LOGIC;
    CH_A_rst : out STD_LOGIC;
    CH_A_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    CH_B_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    CH_B_clk : out STD_LOGIC;
    CH_B_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_B_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    CH_B_en : out STD_LOGIC;
    CH_B_rst : out STD_LOGIC;
    CH_B_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    FFT_RE_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    FFT_RE_clk : out STD_LOGIC;
    FFT_RE_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_RE_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_RE_en : out STD_LOGIC;
    FFT_RE_rst : out STD_LOGIC;
    FFT_RE_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    FFT_IM_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    FFT_IM_clk : out STD_LOGIC;
    FFT_IM_din : out STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_IM_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    FFT_IM_en : out STD_LOGIC;
    FFT_IM_rst : out STD_LOGIC;
    FFT_IM_we : out STD_LOGIC_VECTOR ( 3 downto 0 );
    PS_PL_2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PS_PL_3_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component zed_design;
begin
zed_design_i: component zed_design
     port map (
      CH_A_addr(12 downto 0) => CH_A_addr(12 downto 0),
      CH_A_clk => CH_A_clk,
      CH_A_din(31 downto 0) => CH_A_din(31 downto 0),
      CH_A_dout(31 downto 0) => CH_A_dout(31 downto 0),
      CH_A_en => CH_A_en,
      CH_A_rst => CH_A_rst,
      CH_A_we(3 downto 0) => CH_A_we(3 downto 0),
      CH_B_addr(12 downto 0) => CH_B_addr(12 downto 0),
      CH_B_clk => CH_B_clk,
      CH_B_din(31 downto 0) => CH_B_din(31 downto 0),
      CH_B_dout(31 downto 0) => CH_B_dout(31 downto 0),
      CH_B_en => CH_B_en,
      CH_B_rst => CH_B_rst,
      CH_B_we(3 downto 0) => CH_B_we(3 downto 0),
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FFT_IM_addr(12 downto 0) => FFT_IM_addr(12 downto 0),
      FFT_IM_clk => FFT_IM_clk,
      FFT_IM_din(31 downto 0) => FFT_IM_din(31 downto 0),
      FFT_IM_dout(31 downto 0) => FFT_IM_dout(31 downto 0),
      FFT_IM_en => FFT_IM_en,
      FFT_IM_rst => FFT_IM_rst,
      FFT_IM_we(3 downto 0) => FFT_IM_we(3 downto 0),
      FFT_RE_addr(12 downto 0) => FFT_RE_addr(12 downto 0),
      FFT_RE_clk => FFT_RE_clk,
      FFT_RE_din(31 downto 0) => FFT_RE_din(31 downto 0),
      FFT_RE_dout(31 downto 0) => FFT_RE_dout(31 downto 0),
      FFT_RE_en => FFT_RE_en,
      FFT_RE_rst => FFT_RE_rst,
      FFT_RE_we(3 downto 0) => FFT_RE_we(3 downto 0),
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      PL_PS_0_tri_i(31 downto 0) => pl_ps_0_tri_i(31 downto 0),
      PL_PS_1_tri_i(31 downto 0) => pl_ps_1_tri_i(31 downto 0),
      PS_PL_0_tri_o(31 downto 0) => ps_pl_0_tri_o(31 downto 0),
      PS_PL_1_tri_o(31 downto 0) => ps_pl_1_tri_o(31 downto 0),
      PS_PL_2_tri_o(31 downto 0) => ps_pl_2_tri_o(31 downto 0),
      PS_PL_3_tri_o(31 downto 0) => ps_pl_3_tri_o(31 downto 0)
    );
end STRUCTURE;
