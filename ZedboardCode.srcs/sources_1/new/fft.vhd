library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use IEEE.NUMERIC_STD.ALL;

--FFT Block
entity fft is
	Port (
	--Zugriff von PS auf das BRAM f�r imagin�re Zahlen
	FFT_IM_addr	:	in	STD_LOGIC_VECTOR	(	12	downto	0	);
	FFT_IM_clk	:	in	STD_LOGIC;
	FFT_IM_din	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
	FFT_IM_dout	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
	FFT_IM_en	:	in	STD_LOGIC;
	FFT_IM_rst	:	in	STD_LOGIC;
	FFT_IM_we	:	in	STD_LOGIC_VECTOR	(	3	downto	0	);
	
	--Zugriff von PS auf das BRAM f�r reale Zahlen
	FFT_RE_addr	:	in	STD_LOGIC_VECTOR	(	12	downto	0	);
	FFT_RE_clk	:	in	STD_LOGIC;
	FFT_RE_din	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
	FFT_RE_dout	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
	FFT_RE_en	:	in	STD_LOGIC;
	FFT_RE_rst	:	in	STD_LOGIC;
	FFT_RE_we	:	in	STD_LOGIC_VECTOR	(	3	downto	0	);

	-- FFT Konfiguration
	FFT_CONFIG		: in	std_logic_vector(15 downto 0) := (others => '0');
	
	CLK 			: in	STD_LOGIC := '0';
	start			: in	STD_LOGIC := '0';
	finished		: out	STD_LOGIC := '0'
	);
end fft;

architecture Behavioral of fft is
	COMPONENT xfft_v1
		PORT (
			aclk : IN STD_LOGIC;
			s_axis_config_tdata			: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
			s_axis_config_tvalid		: IN STD_LOGIC;
			s_axis_config_tready		: OUT STD_LOGIC;
			s_axis_data_tdata			: IN STD_LOGIC_VECTOR(63 DOWNTO 0);
			s_axis_data_tvalid			: IN STD_LOGIC;
			s_axis_data_tready			: OUT STD_LOGIC;
			s_axis_data_tlast			: IN STD_LOGIC;
			m_axis_data_tdata			: OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
			m_axis_data_tuser			: OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
			m_axis_data_tvalid			: OUT STD_LOGIC;
			m_axis_data_tready			: IN STD_LOGIC;
			m_axis_data_tlast			: OUT STD_LOGIC;
			m_axis_status_tdata			: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
			m_axis_status_tvalid		: OUT STD_LOGIC;
			m_axis_status_tready		: IN STD_LOGIC;
			event_frame_started			: OUT STD_LOGIC;
			event_tlast_unexpected		: OUT STD_LOGIC;
			event_tlast_missing			: OUT STD_LOGIC;
			event_fft_overflow 			: OUT STD_LOGIC;
			event_status_channel_halt	: OUT STD_LOGIC;
			event_data_in_channel_halt	: OUT STD_LOGIC;
			event_data_out_channel_halt	: OUT STD_LOGIC
		);
	END COMPONENT;
COMPONENT bram_2048_32bit
  PORT (
    clka : IN STD_LOGIC;
    rsta : IN STD_LOGIC;
    ena : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    rstb : IN STD_LOGIC;
    enb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;
	signal data_in_valid : STD_LOGIC := '0';
	signal data_in_ready : STD_LOGIC := '0';
	signal data_in_last : STD_LOGIC := '0';
		
	signal data_out_valid : STD_LOGIC := '0';
	signal data_out_ready : STD_LOGIC := '0';
	signal data_out_last : STD_LOGIC := '0';
	signal m_axis_data_tready : STD_LOGIC := '0';
	
	signal config_valid : STD_LOGIC := '0';
	signal config_ready : STD_LOGIC := '0';
	
	signal counter : integer range 0 to 2047 := 0;
	signal BRAM_we : std_logic_vector(3 downto 0) := (others	=>	'0');
	signal BRAM_addr : std_logic_vector(31 downto 0) := (others	=>	'0');
	
	signal RAM2FFT_ALL : std_logic_vector(63 downto 0) := (others	=>	'0');
	signal RAM2FFT_RE : std_logic_vector(31 downto 0) := (others	=>	'0');
	signal RAM2FFT_IM : std_logic_vector(31 downto 0) := (others	=>	'0');
	
	signal FFT2RAM_ALL : std_logic_vector(63 downto 0) := (others	=>	'0');
	signal FFT2RAM_RE : std_logic_vector(31 downto 0) := (others	=>	'0');
	signal FFT2RAM_IM : std_logic_vector(31 downto 0) := (others	=>	'0');
	
	type fft_state_type is (IDLE, CONFIG, RAM2FFT, WAITING, FFT2RAM);
	signal fft_state : fft_state_type := IDLE;
	
	
begin
bram_im: component bram_2048_32bit
  PORT MAP (
  clka => FFT_IM_clk,
  rsta => FFT_IM_rst,
  ena => FFT_IM_en,
  wea => FFT_IM_we,
  addra => "0000000000000000000" & FFT_IM_addr,
  dina => FFT_IM_din,
  douta => FFT_IM_dout,
  
  clkb => CLK,
  rstb => '0',
  enb => '1',
  web => BRAM_we,
  addrb => BRAM_addr,
  dinb => FFT2RAM_IM,
  doutb => RAM2FFT_IM
);

bram_re: component bram_2048_32bit
  PORT MAP (
  clka => FFT_RE_clk,
  rsta => FFT_RE_rst,
  ena => FFT_RE_en,
  wea => FFT_RE_we,
  addra => "0000000000000000000" & FFT_RE_addr,
  dina => FFT_RE_din,
  douta => FFT_RE_dout,
  
  clkb => CLK,
  rstb => '0',
  enb => '1',
  web => BRAM_we,
  addrb => BRAM_addr,
  dinb => FFT2RAM_RE,
  doutb => RAM2FFT_RE

);

fft_block: component xfft_v1
	port map(
		aclk						=> CLK,
		s_axis_config_tdata			=> FFT_CONFIG (15 downto 0),
		s_axis_config_tvalid		=> config_valid,
		s_axis_config_tready		=> config_ready,
		s_axis_data_tdata			=> RAM2FFT_ALL,
		s_axis_data_tvalid		 	=> data_in_valid,
		s_axis_data_tready		 	=> data_in_ready,
		s_axis_data_tlast			=> data_in_last,
		m_axis_data_tdata			=> FFT2RAM_ALL,
		m_axis_data_tuser			=> open,
		m_axis_data_tvalid		 	=> data_out_valid,
		m_axis_data_tready			=> m_axis_data_tready,
		m_axis_data_tlast			=> data_out_last,
		m_axis_status_tdata			=> open,
		m_axis_status_tvalid		=> open,
		m_axis_status_tready		=> '1',
		event_frame_started			=> open,
		event_tlast_unexpected		=> open,
		event_tlast_missing			=> open,
		event_fft_overflow			=> open,
		event_data_in_channel_halt	=> open,
		event_status_channel_halt 	=> open,
		event_data_out_channel_halt => open
	); 
FFT2RAM_RE <= FFT2RAM_ALL(31 downto 0);
FFT2RAM_IM <= FFT2RAM_ALL(63 downto 32);
RAM2FFT_ALL <= RAM2FFT_IM & RAM2FFT_RE;
transfer2fft: process (CLK) is
begin
	if rising_edge(CLK) then
		case fft_state is 
			when IDLE =>
				BRAM_we <= (others => '0');
				data_in_last <= '0';
				m_axis_data_tready <= '0';
				data_in_valid <= '0';
				if start = '1' then 
					finished <= '0';
					fft_state <= CONFIG;
					counter <= 0;
				end if;
			when CONFIG =>
				BRAM_we <= (others => '0');
				if counter = 0 and config_ready = '1' then 
					config_valid <= '1';
					counter <= 1;
				end if;
				if counter >0 then
					config_valid <= '0';
					counter <= 2;
				end if;
				if counter = 2 then
					counter <= 0;
					fft_state <= RAM2FFT;
				end if;
			when RAM2FFT =>
				BRAM_we <= (others => '0');
				BRAM_addr <= std_logic_vector(to_unsigned(counter, BRAM_addr'length-2)) & "00";
				 if data_in_ready = '1' then
				 	if data_in_valid = '0' then
				 		data_in_valid <= '1';
						counter <= 0;
					else
						data_in_valid <= '1';
						counter <= counter+1;
					end if;
					if counter >= 2047 then
						data_in_last <= '1';
						fft_state <= WAITING;
					end if;
				 end if;
			when WAITING =>
				data_in_valid <= '0';
				 m_axis_data_tready <= '1';
				 data_in_valid <= '0';
				 counter <= 0;
				 if data_out_valid = '1' then
					fft_state <= FFT2RAM;
					counter <= 1;
				 end if;
				 BRAM_we <= (others => '1');
			when FFT2RAM =>
				m_axis_data_tready <= '1';
				BRAM_addr <= std_logic_vector(to_unsigned(counter, BRAM_addr'length-2)) & "00";
				counter <= counter+1;
				 if data_out_last = '1' then
					fft_state <= IDLE;
					finished <= '1';
				 end if;
		end case;
	end if;
end process transfer2fft;

end Behavioral;
