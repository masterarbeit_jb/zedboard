----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.07.2017 12:27:42
-- Design Name: 
-- Module Name: mux_translation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_translation is
	Port (
		MUX_E1_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
		MUX_E2_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
		MUX_S1_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
		MUX_S2_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
		
		MUX_E1_A	:	out	STD_LOGIC_VECTOR	(3	downto	0);
		MUX_E2_A	:	out	STD_LOGIC_VECTOR	(3	downto	0);
		MUX_S1_A	:	out	STD_LOGIC_VECTOR	(3	downto	0);
		MUX_S2_A	:	out	STD_LOGIC_VECTOR	(3	downto	0)
	);
end mux_translation;

architecture Behavioral of mux_translation is
	component	MUX_E_translation	is
		Port	(
			pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
			pin_adress	:	out	STD_LOGIC_VECTOR	(3	downto	0)
		);
	end	component	MUX_E_translation;
	component	MUX_S_translation	is
		Port	(
			pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
			pin_adress	:	out	STD_LOGIC_VECTOR	(3	downto	0)
		);
	end	component	MUX_S_translation;
	
begin
trans_mux_E1: component MUX_E_translation
	port	map(
		pin_number	=>	MUX_E1_pin_number,
		pin_adress	=>	MUX_E1_A
	);

trans_mux_E2: component MUX_E_translation
	port	map(
		pin_number	=>	MUX_E2_pin_number,
		pin_adress	=>	MUX_E2_A
	);

trans_mux_S1: component MUX_S_translation
	port	map(
		pin_number	=>	MUX_S1_pin_number,
		pin_adress	=>	MUX_S1_A
	);

trans_mux_S2: component MUX_S_translation
	port	map(
		pin_number	=>	MUX_S2_pin_number,
		pin_adress	=>	MUX_S2_A
	);

end Behavioral;
