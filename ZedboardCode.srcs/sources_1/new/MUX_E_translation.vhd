----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 17.03.2017 16:45:54
-- Design Name: 
-- Module Name: MUX_E_translation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_E_translation is
    Port ( pin_number : in STD_LOGIC_VECTOR (3 downto 0);
           pin_adress : out STD_LOGIC_VECTOR (3 downto 0));
end MUX_E_translation;

architecture Behavioral of MUX_E_translation is
begin
with pin_number select  pin_adress <=
    "1110" when "0000", --0->14
    "1111" when "0001", --1->15
    "0111" when "0010", --2->7
    "0110" when "0011", --3->6
    "1100" when "0100", --4->12
    "1101" when "0101", --5->13
    "0101" when "0110", --6->5
    "0100" when "0111", --7->4
    "1010" when "1000", --8->10 
    "1011" when "1001", --9->11
    "0011" when "1010", --10->3
    "0010" when "1011", --11->2
    "1000" when "1100", --12->8
    "1001" when "1101", --13->9
    "0001" when "1110", --14->1
    "0000" when "1111"; --15->0
end Behavioral;
