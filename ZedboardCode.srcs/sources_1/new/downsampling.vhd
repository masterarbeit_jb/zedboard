library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

entity downsampling is
	generic(
		divider_bits : integer := 4
	);
	port(
		CLK : in std_logic := '0';
		data_in: in std_logic_vector(13 downto 0) := (others => '0');
		divider: in integer range 0 to 2**divider_bits-1 := 0;
		downsample_valid : out std_logic := '0';
		data_out: out std_logic_vector(15 downto 0) := (others => '0')
	);
end downsampling;

architecture Behavioral of downsampling is
	signal sample_sum:		signed(data_in'length+ 2**divider_bits	downto	0)	:=	(others	=>	'0');
	signal sample_counter:	unsigned(2**divider_bits	downto	0)	:=	(others	=>	'0');
begin
downsampling_p: process(CLK, data_in)
begin
	if rising_edge(CLK) then
		if sample_counter < 2**divider-1 then
			sample_counter <= sample_counter + 1;
			sample_sum <= sample_sum + signed(data_in);
			downsample_valid <= '0';
		else
			sample_counter <= (others => '0');
			downsample_valid <= '1';
			--Teilen durch die von divider vorgegebene Zweierpotenz durch Schiebeoperation nach rechts.
			data_out <= "00" & std_logic_vector(shift_right(sample_sum,divider)(13 downto 0));
			--Summe mit aktuellen Wert Initalisiern.
			sample_sum <= to_signed(to_integer(signed(data_in)), sample_sum'length);
		end if;
	end if;
end process downsampling_p;

end Behavioral;
