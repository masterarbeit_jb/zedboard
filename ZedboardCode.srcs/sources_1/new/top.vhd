----------------------------------------------------------------------------------
--	Company:	
--	Engineer:	
--	
--	Create	Date:	17.03.2017	12:31:13
--	Design	Name:	
--	Module	Name:	top	-	Behavioral
--	Project	Name:	
--	Target	Devices:	
--	Tool	Versions:	
--	Description:	
--	
--	Dependencies:	
--	
--	Revision:
--	Revision	0.01	-	File	Created
--	Additional	Comments:
--	
----------------------------------------------------------------------------------


library	IEEE;
use	IEEE.STD_LOGIC_1164.ALL;

--	Uncomment	the	following	library	declaration	if	using
--	arithmetic	functions	with	Signed	or	Unsigned	values
use	IEEE.NUMERIC_STD.ALL;

--	Uncomment	the	following	library	declaration	if	instantiating
--	any	Xilinx	leaf	cells	in	this	code.
--library	UNISIM;
--use	UNISIM.VComponents.all;

entity	top	is
	Port	(
		GCLK	:	in	std_logic;
		SW	:	in	std_logic_vector	(7	downto	0);
		LED	:	out	std_logic_vector	(7	downto	0);
		
		FMC_INPUT_A_P	:	in	std_logic_vector(6	downto	0);
		FMC_INPUT_A_N	:	in	std_logic_vector(6	downto	0);
		FMC_INPUT_B_P	:	in	std_logic_vector(6	downto	0);
		FMC_INPUT_B_N	:	in	std_logic_vector(6	downto	0);
		
		FMC_INPUT_CLK_P	:	in	std_logic;
		FMC_INPUT_CLK_N	:	in	std_logic;
		
		MUX_S1_A	:	out	std_logic_vector(3	downto	0);
		MUX_S2_A	:	out	std_logic_vector(3	downto	0);
		MUX_E1_A	:	out	std_logic_vector(3	downto	0);
		MUX_E2_A	:	out	std_logic_vector(3	downto	0);
		MUX_V_A	:	out	std_logic_vector(3	downto	0);
		
		MUX_S1_EN	:	out	std_logic;
		MUX_S2_EN	:	out	std_logic;
		MUX_E1_EN	:	out	std_logic;
		MUX_E2_EN	:	out	std_logic;
		MUX_V_EN	:	out	std_logic;
	
		DDR_cas_n	:	inout	STD_LOGIC;
		DDR_cke	:	inout	STD_LOGIC;
		DDR_ck_n	:	inout	STD_LOGIC;
		DDR_ck_p	:	inout	STD_LOGIC;
		DDR_cs_n	:	inout	STD_LOGIC;
		DDR_reset_n	:	inout	STD_LOGIC;
		DDR_odt	:	inout	STD_LOGIC;
		DDR_ras_n	:	inout	STD_LOGIC;
		DDR_we_n	:	inout	STD_LOGIC;
		DDR_ba	:	inout	STD_LOGIC_VECTOR	(	2	downto	0	);
		DDR_addr	:	inout	STD_LOGIC_VECTOR	(	14	downto	0	);
		DDR_dm	:	inout	STD_LOGIC_VECTOR	(	3	downto	0	);
		DDR_dq	:	inout	STD_LOGIC_VECTOR	(	31	downto	0	);
		DDR_dqs_n	:	inout	STD_LOGIC_VECTOR	(	3	downto	0	);
		DDR_dqs_p	:	inout	STD_LOGIC_VECTOR	(	3	downto	0	);
		
		FIXED_IO_mio	:	inout	STD_LOGIC_VECTOR	(	53	downto	0	);
		FIXED_IO_ddr_vrn	:	inout	STD_LOGIC;
		FIXED_IO_ddr_vrp	:	inout	STD_LOGIC;
		FIXED_IO_ps_srstb	:	inout	STD_LOGIC;
		FIXED_IO_ps_clk	:	inout	STD_LOGIC;
		FIXED_IO_ps_porb	:	inout	STD_LOGIC
	);
end	top;

architecture	Behavioral	of	top	is
	component	zed_design	is
		port (
			DDR_cas_n	:	inout	STD_LOGIC;
			DDR_cke	:	inout	STD_LOGIC;
			DDR_ck_n	:	inout	STD_LOGIC;
			DDR_ck_p	:	inout	STD_LOGIC;
			DDR_cs_n	:	inout	STD_LOGIC;
			DDR_reset_n	:	inout	STD_LOGIC;
			DDR_odt	:	inout	STD_LOGIC;
			DDR_ras_n	:	inout	STD_LOGIC;
			DDR_we_n	:	inout	STD_LOGIC;
			DDR_ba	:	inout	STD_LOGIC_VECTOR	(	2	downto	0	);
			DDR_addr	:	inout	STD_LOGIC_VECTOR	(	14	downto	0	);
			DDR_dm	:	inout	STD_LOGIC_VECTOR	(	3	downto	0	);
			DDR_dq	:	inout	STD_LOGIC_VECTOR	(	31	downto	0	);
			DDR_dqs_n	:	inout	STD_LOGIC_VECTOR	(	3	downto	0	);
			DDR_dqs_p	:	inout	STD_LOGIC_VECTOR	(	3	downto	0	);
			FIXED_IO_mio	:	inout	STD_LOGIC_VECTOR	(	53	downto	0	);
			FIXED_IO_ddr_vrn	:	inout	STD_LOGIC;
			FIXED_IO_ddr_vrp	:	inout	STD_LOGIC;
			FIXED_IO_ps_srstb	:	inout	STD_LOGIC;
			FIXED_IO_ps_clk	:	inout	STD_LOGIC;
			FIXED_IO_ps_porb	:	inout	STD_LOGIC;
			PS_PL_0_tri_o	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
			PS_PL_1_tri_o	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
			PL_PS_0_tri_i	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
			PL_PS_1_tri_i	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
			CH_A_addr	:	out	STD_LOGIC_VECTOR	(	12	downto	0	);
			CH_A_clk	:	out	STD_LOGIC;
			CH_A_din	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
			CH_A_dout	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
			CH_A_en	:	out	STD_LOGIC;
			CH_A_rst	:	out	STD_LOGIC;
			CH_A_we	:	out	STD_LOGIC_VECTOR	(	3	downto	0	);
			CH_B_addr	:	out	STD_LOGIC_VECTOR	(	12	downto	0	);
			CH_B_clk	:	out	STD_LOGIC;
			CH_B_din	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
			CH_B_dout	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
			CH_B_en	:	out	STD_LOGIC;
			CH_B_rst	:	out	STD_LOGIC;
			CH_B_we	:	out	STD_LOGIC_VECTOR	(	3	downto	0	);
			FFT_IM_addr	:	out	STD_LOGIC_VECTOR	(	12	downto	0	);
			FFT_IM_clk	:	out	STD_LOGIC;
			FFT_IM_din	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
			FFT_IM_dout	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
			FFT_IM_en	:	out	STD_LOGIC;
			FFT_IM_rst	:	out	STD_LOGIC;
			FFT_IM_we	:	out	STD_LOGIC_VECTOR	(	3	downto	0	);
			FFT_RE_addr	:	out	STD_LOGIC_VECTOR	(	12	downto	0	);
			FFT_RE_clk	:	out	STD_LOGIC;
			FFT_RE_din	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
			FFT_RE_dout	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
			FFT_RE_en	:	out	STD_LOGIC;
			FFT_RE_rst	:	out	STD_LOGIC;
			FFT_RE_we	:	out	STD_LOGIC_VECTOR	(	3	downto	0	)
		);
	end	component	zed_design;

	component	selectio_wiz_0
		generic	(
			--	width	of	the	data	for	the	system
			SYS_W:	integer	:=	14;
			--	width	of	the	data	for	the	device
			DEV_W:	integer	:=	28
		);
		port	(
			--	From	the	system	into	the	device
			data_in_from_pins_p	:	in	std_logic_vector(SYS_W-1	downto	0);
			data_in_from_pins_n	:	in	std_logic_vector(SYS_W-1	downto	0);
			data_in_to_device:	out	std_logic_vector(DEV_W-1	downto	0);
			
			--	Clock	and	reset	signals
			clk_in_p:	in	std_logic;--	Differential	fast	clock	from	IOB
			clk_in_n:	in	std_logic;
			clk_out	:	out	std_logic;
			io_reset:	in	std_logic
		);--	Reset	signal	for	IO	circuit
	end	component;
	
	component CHdata is
	generic(
		downsamplingbits : integer := 4
	);
	Port (
		CLK : in std_logic;
		chraw : IN std_logic_vector (13 downto 0);
		data_request : in std_logic;
		data_ready : out std_logic;
		
		downsampling: in integer range 0 to 2**downsamplingbits-1 := 0; -- Range muss weiter unten auch angepasst werden...
		
		PS_CLK : in std_logic;
		PS_addr : in std_logic_vector(12 downto 0) ;
		PS_dout : out std_logic_vector (31 downto 0)
	);
	end component CHdata;

	COMPONENT	ADC_sync	
		PORT	(
			--CLK	vom	Core
			GCLK	:	in	STD_LOGIC;
			--CLK	vom	ADC
			ADC_CLK	:	in	STD_LOGIC;
			--	Datenbus	vom	ADC
			ADC_SDR	:	in	STD_LOGIC_VECTOR	(27	downto	0);
			
			clk_in_p:	in	std_logic;--	Differential	fast	clock	from	IOB
			clk_in_n:	in	std_logic;
			data_in_from_pins_p	:	in	std_logic_vector(13	downto	0);
        	data_in_from_pins_n	:	in	std_logic_vector(13	downto	0);
        	
			--	Daten,	synchron	zu	G_CLK	zur	weiteren	Verarbeitung
			ADC_A	:	out	STD_LOGIC_VECTOR	(13	downto	0);
			ADC_B	:	out	STD_LOGIC_VECTOR	(13	downto	0)
		);
	end	COMPONENT;
	
	COMPONENT fft
	Port (
		FFT_IM_addr	:	in	STD_LOGIC_VECTOR	(	12	downto	0	);
		FFT_IM_clk	:	in	STD_LOGIC;
		FFT_IM_din	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
		FFT_IM_dout	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
		FFT_IM_en	:	in	STD_LOGIC;
		FFT_IM_rst	:	in	STD_LOGIC;
		FFT_IM_we	:	in	STD_LOGIC_VECTOR	(	3	downto	0	);
		FFT_RE_addr	:	in	STD_LOGIC_VECTOR	(	12	downto	0	);
		FFT_RE_clk	:	in	STD_LOGIC;
		FFT_RE_din	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
		FFT_RE_dout	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
		FFT_RE_en	:	in	STD_LOGIC;
		FFT_RE_rst	:	in	STD_LOGIC;
		FFT_RE_we	:	in	STD_LOGIC_VECTOR	(	3	downto	0	);
			
		FFT_CONFIG	:	in	std_logic_vector(15	downto 0) := (others => '0');
		
		CLK			:	in	STD_LOGIC	:=	'0';
		start		:	in	STD_LOGIC	:=	'0';
		finished	:	out	STD_LOGIC	:=	'0'
	);
	end	COMPONENT;
	component mux_translation is
		Port (
			MUX_E1_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
			MUX_E2_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
			MUX_S1_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
			MUX_S2_pin_number	:	in	STD_LOGIC_VECTOR	(3	downto	0);
			
			MUX_E1_A	:	out	STD_LOGIC_VECTOR	(3	downto	0);
			MUX_E2_A	:	out	STD_LOGIC_VECTOR	(3	downto	0);
			MUX_S1_A	:	out	STD_LOGIC_VECTOR	(3	downto	0);
			MUX_S2_A	:	out	STD_LOGIC_VECTOR	(3	downto	0)
		);
	end component mux_translation;
	
	signal	PS_PL_0	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	signal	PS_PL_1	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	signal	PS_PL_2	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	
	signal	PL_PS_0	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	signal	PL_PS_1	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	
	signal	downsampling	:	integer range 0 to 15	:=	0;
	signal	data_request	:	std_logic	:=	'0';
	signal	data_ready_A	:	std_logic	:=	'0';
	signal	data_ready_B	:	std_logic	:=	'0';
	
	signal	ADC_SDR	:	std_logic_vector(27	downto	0)	:=	(others	=>	'0');
	signal	CLK_ADC	:	STD_LOGIC	:=	'0';
	signal	ADC_A	:	STD_LOGIC_VECTOR	(	13	downto	0	)	:=	(others	=>	'0');
	signal	ADC_B	:	STD_LOGIC_VECTOR	(	13	downto	0	)	:=	(others	=>	'0');
	

	signal	MUX_S1_pin_number	:	std_logic_vector(3	downto	0)	:=	(others	=>	'0');
	signal	MUX_S2_pin_number	:	std_logic_vector(3	downto	0)	:=	(others	=>	'0');
	signal	MUX_E1_pin_number	:	std_logic_vector(3	downto	0)	:=	(others	=>	'0');
	signal	MUX_E2_pin_number	:	std_logic_vector(3	downto	0)	:=	(others	=>	'0');
	
	signal	CH_A_addr	:	std_logic_vector(12	downto	0)	:=	(others	=>	'0');
	signal	CH_A_clk	:	std_logic	:=	'0';
	signal	CH_A_din	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	signal	CH_A_dout	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	signal	CH_A_en	:	std_logic	:=	'0';
	signal	CH_A_rst	:	std_logic	:=	'0';
	signal	CH_A_we	:	std_logic_vector(3	downto	0)	:=	(others	=>	'0');
	
	signal	CH_B_addr	:	std_logic_vector(12	downto	0)	:=	(others	=>	'0');
	signal	CH_B_clk	:	std_logic	:=	'0';
	signal	CH_B_din	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	signal	CH_B_dout	:	std_logic_vector(31	downto	0)	:=	(others	=>	'0');
	signal	CH_B_en	:	std_logic	:=	'0';
	signal	CH_B_rst	:	std_logic	:=	'0';
	signal	CH_B_we	:	std_logic_vector(3	downto	0)	:=	(others	=>	'0');
	
	signal	FFT_start		: std_logic	:=	'0';
	signal	FFT_finished	: std_logic	:=	'0';
	signal	FFT_CONFIG		: std_logic_vector(15	downto	0)	:=	(others	=>	'0');
	
	signal FFT_IM_addr		: std_logic_vector(12 downto 0)	:=	(others	=>	'0');
	signal FFT_IM_clk		: std_logic 						:=	'0'; 
	signal FFT_IM_din		: std_logic_vector(31 downto 0)	:=	(others	=>	'0');
	signal FFT_IM_dout		: std_logic_vector(31 downto 0)	:=	(others	=>	'0');
	signal FFT_IM_en		: std_logic 						:=	'0'; 
	signal FFT_IM_rst		: std_logic 						:=	'0'; 
	signal FFT_IM_we		: std_logic_vector(3 downto 0)	:=	(others	=>	'0');
	
	signal FFT_RE_addr		: std_logic_vector(12 downto 0)	:=	(others	=>	'0');
	signal FFT_RE_clk		: std_logic 						:=	'0'; 
	signal FFT_RE_din		: std_logic_vector(31 downto 0)	:=	(others	=>	'0');
	signal FFT_RE_dout		: std_logic_vector(31 downto 0)	:=	(others	=>	'0');
	signal FFT_RE_en		: std_logic						:=	'0'; 
	signal FFT_RE_rst		: std_logic 						:=	'0'; 
	signal FFT_RE_we		: std_logic_vector(3	downto	0)	:=	(others	=>	'0');
begin
-- Zuweisung der PS und PL Pins in Klartext
MUX_E1_pin_number	<= PS_PL_0(3 downto 0);
MUX_E2_pin_number	<= PS_PL_0(7 downto 4);
MUX_S1_pin_number	<= PS_PL_0(11 downto 8);
MUX_S2_pin_number	<= PS_PL_0(15 downto 12);
MUX_V_A				<= PS_PL_0(19 downto 16);
MUX_E1_EN			<= PS_PL_0(20);
MUX_E2_EN			<= PS_PL_0(21);
MUX_S1_EN			<= PS_PL_0(22);
MUX_S2_EN			<= PS_PL_0(23);
MUX_V_EN			<= PS_PL_0(24);
downsampling		<= to_integer(unsigned(PS_PL_0(28 downto 25)));
data_request		<= PS_PL_0(29);

FFT_start			<= PS_PL_0(30);

LED(7 downto 0)		<= PS_PL_1(7 downto 0);
FFT_CONFIG			<= PS_PL_1(23 downto 8);

PL_PS_0(7 downto 0)	<= SW;
PL_PS_0(8)			<= data_ready_A and data_ready_B;
PL_PS_0(9)			<= FFT_finished;
-- ENDE Zuweisung der PS und PL Pins in Klartext

zed: component zed_design
		port map	(
		CH_A_addr(12 downto 0)		=> CH_A_addr(12 downto 0),
		CH_A_clk					=> CH_A_clk,
		CH_A_din(31 downto 0)		=> CH_A_din(31 downto 0),
		CH_A_dout(31 downto 0)		=> CH_A_dout(31 downto 0),
		CH_A_en						=> CH_A_en,
		CH_A_rst					=> CH_A_rst,
		CH_A_we(3 downto 0)			=> CH_A_we(3 downto 0),
		CH_B_addr(12 downto 0)		=> CH_B_addr(12 downto 0),
		CH_B_clk					=> CH_B_clk,
		CH_B_din(31 downto 0)		=> CH_B_din(31 downto 0),
		CH_B_dout(31 downto 0)		=> CH_B_dout(31 downto 0),
		CH_B_en						=> CH_B_en,
		CH_B_rst					=> CH_B_rst,
		CH_B_we(3 downto 0)			=> CH_B_we(3 downto 0),
		DDR_addr(14	downto 0)		=> DDR_addr(14 downto 0),
		DDR_ba(2 downto 0)			=> DDR_ba(2 downto 0),
		DDR_cas_n					=> DDR_cas_n,
		DDR_ck_n					=> DDR_ck_n,
		DDR_ck_p					=> DDR_ck_p,
		DDR_cke						=> DDR_cke,
		DDR_cs_n					=> DDR_cs_n,
		DDR_dm(3 downto	0)			=> DDR_dm(3 downto 0),
		DDR_dq(31 downto 0)			=> DDR_dq(31 downto 0),
		DDR_dqs_n(3	downto 0)		=> DDR_dqs_n(3 downto 0),
		DDR_dqs_p(3	downto 0)		=> DDR_dqs_p(3 downto 0),
		DDR_odt						=> DDR_odt,
		DDR_ras_n					=> DDR_ras_n,
		DDR_reset_n					=> DDR_reset_n,
		DDR_we_n					=> DDR_we_n,

		FIXED_IO_ddr_vrn			=> FIXED_IO_ddr_vrn,
		FIXED_IO_ddr_vrp			=> FIXED_IO_ddr_vrp,
		FIXED_IO_mio(53 downto 0)	=> FIXED_IO_mio(53 downto 0),
		FIXED_IO_ps_clk				=> FIXED_IO_ps_clk,
		FIXED_IO_ps_porb			=> FIXED_IO_ps_porb,
		FIXED_IO_ps_srstb			=> FIXED_IO_ps_srstb,
		PL_PS_0_tri_i(31 downto 0)	=> PL_PS_0,
		PL_PS_1_tri_i(31 downto 0)	=> PL_PS_1,
		PS_PL_0_tri_o(31 downto 0)	=> PS_PL_0,
		PS_PL_1_tri_o(31 downto 0)	=> PS_PL_1,
		
		FFT_IM_addr(12 downto 0)	=> FFT_IM_addr(12 downto 0),
		FFT_IM_clk					=> FFT_IM_clk,
		FFT_IM_din(31 downto 0)		=> FFT_IM_din(31 downto 0),
		FFT_IM_dout(31 downto 0)	=> FFT_IM_dout(31 downto 0),
		FFT_IM_en					=> FFT_IM_en,
		FFT_IM_rst					=> FFT_IM_rst,
		FFT_IM_we(3 downto 0)		=> FFT_IM_we(3 downto 0),
		FFT_RE_addr(12 downto 0)	=> FFT_RE_addr(12 downto 0),
		FFT_RE_clk					=> FFT_RE_clk,
		FFT_RE_din(31 downto 0)		=> FFT_RE_din(31 downto 0),
		FFT_RE_dout(31 downto 0)	=> FFT_RE_dout(31 downto 0),
		FFT_RE_en					=> FFT_RE_en,
		FFT_RE_rst					=> FFT_RE_rst,
		FFT_RE_we(3	downto	0)		=> FFT_RE_we(3 downto 0)
);

ADC_read_data:	ADC_sync
port map(
	--CLK	vom	Core
	GCLK	=>	GCLK,
	--CLK	vom	ADC
	ADC_CLK	=>	CLK_ADC,
	clk_in_p			=> FMC_INPUT_CLK_P,
	clk_in_n			=> FMC_INPUT_CLK_N,
	--	Daten	vom	ADC
	ADC_SDR	=>	ADC_SDR,
	data_in_from_pins_p	=> FMC_INPUT_A_P & FMC_INPUT_B_P,
	data_in_from_pins_n	=> FMC_INPUT_A_N & FMC_INPUT_B_N,
	--	Daten,	synchron	zu	G_CLK	zur	weiteren	Verarbeitung
	ADC_A	=>	ADC_A,
	ADC_B	=>	ADC_B
	);
	
CHA: CHdata
generic map (
	downsamplingbits	=>	4)
port map (
	CLK				=> GCLK,
	chraw			=> ADC_A,
	data_request	=> data_request,
	data_ready		=> data_ready_A,
	
	downsampling	=> downsampling,
	
	PS_CLK			=> CH_A_clk,
	PS_addr			=> CH_A_addr,
	PS_dout			=> CH_A_dout
);
CHB: CHdata
generic map (
	downsamplingbits	=>	4)
port map (
	CLK 			=> GCLK,
	chraw 			=> ADC_B,
	data_request 	=> data_request,
	data_ready 		=> data_ready_B,
	
	downsampling	=> downsampling,
	
	PS_CLK 			=> CH_B_clk,
	PS_addr 		=> CH_B_addr,
	PS_dout 		=> CH_B_dout
);

FFT_comp: fft
port map(
	FFT_IM_addr(12 downto 0)	=> FFT_IM_addr(12 downto 0),
	FFT_IM_clk					=> FFT_IM_clk,
	FFT_IM_din(31 downto 0)		=> FFT_IM_din(31 downto 0),
	FFT_IM_dout(31 downto 0)	=> FFT_IM_dout(31 downto 0),
	FFT_IM_en					=> FFT_IM_en,
	FFT_IM_rst					=> FFT_IM_rst,
	FFT_IM_we(3 downto 0)		=> FFT_IM_we(3 downto 0),
	
	FFT_RE_addr(12 downto 0)	=> FFT_RE_addr(12 downto 0),
	FFT_RE_clk					=> FFT_RE_clk,
	FFT_RE_din(31 downto 0)		=> FFT_RE_din(31 downto 0),
	FFT_RE_dout(31 downto 0)	=> FFT_RE_dout(31 downto 0),
	FFT_RE_en					=> FFT_RE_en,
	FFT_RE_rst					=> FFT_RE_rst,
	FFT_RE_we(3 downto 0)		=> FFT_RE_we(3 downto 0),

	FFT_CONFIG					=> FFT_CONFIG,

	CLK							=> GCLK,
	start						=> FFT_start,
	finished					=> FFT_finished
);
muxtranslation: mux_translation 
port map(
	MUX_E1_pin_number => MUX_E1_pin_number,	
	MUX_E2_pin_number => MUX_E2_pin_number,
	MUX_S1_pin_number => MUX_S1_pin_number,
	MUX_S2_pin_number => MUX_S2_pin_number,
		
	MUX_E1_A => MUX_E1_A, 
	MUX_E2_A => MUX_E2_A,	
	MUX_S1_A => MUX_S1_A,
	MUX_S2_A => MUX_S2_A
);
end	Behavioral;