----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.03.2017 11:45:13
-- Design Name: 
-- Module Name: MUX_S_translation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_S_translation is
    Port ( pin_number : in STD_LOGIC_VECTOR (3 downto 0);
           pin_adress : out STD_LOGIC_VECTOR (3 downto 0));
end MUX_S_translation;

architecture Behavioral of MUX_S_translation is

begin
with pin_number select  pin_adress <=
    "0110" when "0000", --0-> 6
    "0100" when "0001", --1-> 4
    "0010" when "0010", --2-> 2
    "0000" when "0011", --3-> 0
    "0111" when "0100", --4-> 7
    "0101" when "0101", --5-> 5
    "0011" when "0110", --6-> 3
    "0001" when "0111", --7-> 1
    "1111" when "1000", --8-> 15
    "1101" when "1001", --9-> 13
    "1011" when "1010", --10->11
    "1001" when "1011", --11->9
    "1110" when "1100", --12->14
    "1100" when "1101", --13->12
    "1010" when "1110", --14->10
    "1000" when "1111"; --15->8
end Behavioral;
