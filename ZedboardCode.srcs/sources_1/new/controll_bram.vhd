----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.07.2017 14:13:03
-- Design Name: 
-- Module Name: controll_bram - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity controll_bram is
	Port (
		CLK : in std_logic := '0';
		data_request : in std_logic := '0';
		data_ready : out std_logic := '0';
		data_in_valid : in std_logic := '0';
		data_out_valid : out std_logic_vector(0	downto	0)	:=	(others	=>	'0');
		adr_counter : buffer  unsigned(10 downto 0)	:=	(others	=>	'0')
	);
end controll_bram;

architecture Behavioral of controll_bram is
	type collect_and_save_state_type	is	(IDLE,	COLLECT,	READY);
	signal collect_and_save_state	:	collect_and_save_state_type	:=	IDLE;
begin
collect_and_save_data:	process(CLK)
begin
	if	rising_edge(CLK)	then
		case collect_and_save_state is	
			when IDLE =>
				data_ready	<=	'0';
				if	data_request	=	'1'	then
					collect_and_save_state	<=	COLLECT;
					adr_counter	<=	to_unsigned(0,adr_counter'length);
					
				end	if;
			when COLLECT =>
				data_ready	<=	'0';
				if data_in_valid = '1' then
					adr_counter	<=	adr_counter	+	1;
					data_out_valid	<=	"1";
					if	adr_counter	=	2047	then
						collect_and_save_state	<=	READY;
						data_out_valid	<=	"0";
					end	if;
				else
					data_out_valid	<=	"0";
				end if;
				
			when READY =>
				adr_counter	<=	to_unsigned(0,adr_counter'length);
				data_ready	<=	'1';
				if	data_request	=	'1'	then
					collect_and_save_state	<=	COLLECT;
					adr_counter	<=	to_unsigned(0,adr_counter'length);
					data_out_valid	<=	"1";
					data_ready	<=	'0';
				end	if;
		end	case;
	end	if;
end	process	collect_and_save_data;

end Behavioral;
