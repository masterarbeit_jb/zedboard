
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;
-- Nimmt die Daten, ff�hrt im Block downsampling eine verringerung des Downsamplings durch.
-- Wandelt die Daten in Float um.
-- 
entity CHdata is
	Port (
		CLK : in std_logic;
		chraw : IN std_logic_vector (13 downto 0);
		data_request : in std_logic;
		data_ready : out std_logic;
		
		downsampling_div: in integer range 0 to 15 := 0;
		
		PS_CLK : in std_logic;
		PS_addr : in std_logic_vector(12 downto 0) ;
		PS_dout : out std_logic_vector (31 downto 0)
	);
end CHdata;

architecture Behavioral of CHdata is
	COMPONENT	floating_point_0
		PORT	(
			aclk					: IN STD_LOGIC;
			s_axis_a_tvalid			: IN STD_LOGIC;
			s_axis_a_tdata			: IN STD_LOGIC_VECTOR(15 DOWNTO 0);
			m_axis_result_tvalid	: OUT STD_LOGIC;
			m_axis_result_tdata		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
		);
	END	COMPONENT;
	-- Tiefpassfilterung zur Anpassung der Daten.
	COMPONENT variable_lowpass
	  PORT (
	    aclk : IN STD_LOGIC;
	    s_axis_data_tvalid : IN STD_LOGIC;
	    s_axis_data_tready : OUT STD_LOGIC;
	    s_axis_data_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	    m_axis_data_tvalid : OUT STD_LOGIC;
	    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
	  );
	END COMPONENT;
	COMPONENT	block_ram_2048
		PORT	(
			clka	:	IN	STD_LOGIC;
			ena	:	IN	STD_LOGIC;
			wea	:	IN	STD_LOGIC_VECTOR(0	DOWNTO	0);
			addra	:	IN	STD_LOGIC_VECTOR(10	DOWNTO	0);
			dina	:	IN	STD_LOGIC_VECTOR(31	DOWNTO	0);
			clkb	:	IN	STD_LOGIC;
			addrb	:	IN	STD_LOGIC_VECTOR(10	DOWNTO	0);
			doutb	:	OUT	STD_LOGIC_VECTOR(31	DOWNTO	0)
		);	
	END	COMPONENT;
	component downsampling is
		generic(
			divider_bits : integer := 4
		);
		port(
			CLK : in std_logic := '0';
			data_in: in std_logic_vector(13 downto 0) := (others => '0');
			divider: in integer range 0 to 2**divider_bits-1 := 0; -- Range muss weiter unten auch angepasst werden...
			downsample_valid : out std_logic := '0';
			data_out: out std_logic_vector(15 downto 0) := (others => '0')
		);
	end component downsampling;
	component controll_bram is
		Port (
			CLK : in std_logic := '0';
			data_request : in std_logic := '0';
			data_ready : out std_logic := '0';
			data_in_valid : in std_logic := '0';
			data_out_valid : out std_logic_vector(0	downto	0)	:=	(others	=>	'0');
			adr_counter : buffer  unsigned(10 downto 0)	:=	(others	=>	'0')
		);
	end component controll_bram;
	COMPONENT cic_decimation
	  PORT (
	    aclk : IN STD_LOGIC;
	    s_axis_config_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	    s_axis_config_tvalid : IN STD_LOGIC;
	    s_axis_config_tready : OUT STD_LOGIC;
	    s_axis_data_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	    s_axis_data_tvalid : IN STD_LOGIC;
	    s_axis_data_tready : OUT STD_LOGIC;
	    m_axis_data_tdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	    m_axis_data_tvalid : OUT STD_LOGIC
	  );
	END COMPONENT;


	signal CHraw16bit : std_logic_vector (15 downto 0);
	signal CHfloat	:	STD_LOGIC_VECTOR(31	downto	0)	:=	(others	=>	'0');
	signal RAM_enable	:	std_logic_vector(0	downto	0)	:=	(others	=>	'0');
	
	signal adr_counter:	unsigned(10	downto	0)	:=	(others	=>	'0');
	
	signal downsample_valid : std_logic := '0';
	signal floating_valid : std_logic := '0';

	
	signal state1_valid : std_logic := '0';
	signal state1_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state2_valid : std_logic := '0';
	signal state2_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state3_valid : std_logic := '0';
	signal state3_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state4_valid : std_logic := '0';
	signal state4_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state5_valid : std_logic := '0';
	signal state5_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state6_valid : std_logic := '0';
	signal state6_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state7_valid : std_logic := '0';
	signal state7_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	
	signal state8_valid : std_logic := '0';
	signal state8_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state9_valid : std_logic := '0';
	signal state9_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state10_valid : std_logic := '0';
	signal state10_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state11_valid : std_logic := '0';
	signal state11_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state12_valid : std_logic := '0';
	signal state12_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state13_valid : std_logic := '0';
	signal state13_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state14_valid : std_logic := '0';
	signal state14_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
	signal state15_valid : std_logic := '0';
	signal state15_output : STD_LOGIC_VECTOR(15 DOWNTO 0) := (others => '0');
		
begin
-- Tiefpass f�rt zu unbekannten fehlern 
--decimation: process (CLK, downsampling_div)
--begin
--	case downsampling_div is
--		when 0 =>
--			CHraw16bit <= "00" & chraw;
--			downsample_valid <= '1';
--		when 1 =>
--			CHraw16bit <= state1_output;
--			downsample_valid <= state1_valid;
--		when 2 =>
--			CHraw16bit <= state2_output;
--			downsample_valid <= state2_valid;
--		when 3 =>
--			CHraw16bit <= state3_output;
--			downsample_valid <= state3_valid;
--		when 4 =>
--			CHraw16bit <= state4_output;
--			downsample_valid <= state4_valid;
--		when 5 =>
--			CHraw16bit <= state5_output;
--			downsample_valid <= state5_valid;
--		when 6 =>
--			CHraw16bit <= state6_output;
--			downsample_valid <= state6_valid;
--		when 7 =>
--			CHraw16bit <= state7_output;
--			downsample_valid <= state7_valid;
--		when 8 =>
--			CHraw16bit <= state1_output;
--			downsample_valid <= state8_valid;
--		when 9 =>
--			CHraw16bit <= state1_output;
--			downsample_valid <= state9_valid;
--		when 10 =>
--			CHraw16bit <= state10_output;
--			downsample_valid <= state10_valid;
--		when 11 =>
--			CHraw16bit <= state11_output;
--			downsample_valid <= state11_valid;
--		when 12 to 15 =>
--			CHraw16bit <= state12_output;
--			downsample_valid <= state12_valid;
----		when 13 to 15 =>
----			CHraw16bit <= state13_output;
----			downsample_valid <= state13_valid;
----		when 14 =>
----			CHraw16bit <= state14_output;
----			downsample_valid <= state14_valid;
----		when 15 =>
----			CHraw16bit <= state15_output;
----			downsample_valid <= state15_valid;												
--	end case;
--end process decimation;
--decimation_stage1 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => '1',
--    s_axis_data_tready => open,
--    s_axis_data_tdata => "00" & chraw,
--    m_axis_data_tvalid => state1_valid,
--    m_axis_data_tdata => state1_output
--);
--decimation_stage2 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state1_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state1_output,
--    m_axis_data_tvalid => state2_valid,
--    m_axis_data_tdata => state2_output
--);
--decimation_stage3 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state2_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state2_output,
--    m_axis_data_tvalid => state3_valid,
--    m_axis_data_tdata => state3_output
--);
--decimation_stage4 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state3_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state3_output,
--    m_axis_data_tvalid => state4_valid,
--    m_axis_data_tdata => state4_output
--);
--decimation_stage5 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state4_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state4_output,
--    m_axis_data_tvalid => state5_valid,
--    m_axis_data_tdata => state5_output
--);
--decimation_stage6 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state5_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state5_output,
--    m_axis_data_tvalid => state6_valid,
--    m_axis_data_tdata => state6_output
--);
--decimation_stage7 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state6_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state6_output,
--    m_axis_data_tvalid => state7_valid,
--    m_axis_data_tdata => state7_output
--);
--decimation_stage8 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state7_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state7_output,
--    m_axis_data_tvalid => state8_valid,
--    m_axis_data_tdata => state8_output
--);
--decimation_stage9 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state8_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state8_output,
--    m_axis_data_tvalid => state9_valid,
--    m_axis_data_tdata => state9_output
--);
--decimation_stage10 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state9_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state9_output,
--    m_axis_data_tvalid => state10_valid,
--    m_axis_data_tdata => state10_output
--);
--decimation_stage11 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state10_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state10_output,
--    m_axis_data_tvalid => state11_valid,
--    m_axis_data_tdata => state11_output
--);
--decimation_stage12 : variable_lowpass
--  PORT MAP (
--    aclk => CLK,
--    s_axis_data_tvalid => state11_valid,
--    s_axis_data_tready => open,
--    s_axis_data_tdata => state11_output,
--    m_axis_data_tvalid => state12_valid,
--    m_axis_data_tdata => state12_output
--);
mydown: component downsampling
	port map (
		CLK => CLK,
		data_in => chraw,
		divider => downsampling_div,
		downsample_valid => downsample_valid,
		data_out=> CHraw16bit
	);
int_to_float: component floating_point_0
	port	map(
		aclk					=>	CLK,
		s_axis_a_tvalid			=>	downsample_valid,
		s_axis_a_tdata			=>	CHraw16bit,
		m_axis_result_tvalid	=>	floating_valid,
		m_axis_result_tdata		=>	CHfloat
	);
bramcontrol: component controll_bram
	Port map(
		CLK => CLK,
		data_request => data_request,
		data_ready  => data_ready,
		data_in_valid => floating_valid,
		data_out_valid =>RAM_enable,
		adr_counter => adr_counter
	);

BRAM: block_ram_2048
		port	map	(
			clka	=>	CLK,
			ena		=>	RAM_enable(0),
			wea		=>	RAM_enable,	--wea	ist	ein	STD_LOGIC_VECTOR	mit	der	L�nge	1.
			addra	=>	std_logic_vector(adr_counter),
			dina	=>	CHfloat,
			
			clkb	=>	PS_CLK,
			addrb	=>	PS_addr(12	downto	2),
			doutb	=>	PS_dout
		);

end Behavioral;
