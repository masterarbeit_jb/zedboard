----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 22.03.2017 10:02:15
-- Design Name: 
-- Module Name: convert_channel - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity convert_channel is
    Port ( CLK : in STD_LOGIC;
    ADC_in : in STD_LOGIC_VECTOR (13 downto 0) := (others => '0');
    timescale : in STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
    ADC_out : out STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
    READY : out STD_LOGIC);
end convert_channel;

architecture Behavioral of convert_channel is
COMPONENT floating_point_0
  PORT (
    aclk : IN STD_LOGIC;
    aclken : IN STD_LOGIC;
    s_axis_a_tvalid : IN STD_LOGIC;
    s_axis_a_tdata : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_result_tvalid : OUT STD_LOGIC;
    m_axis_result_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;
COMPONENT adc_scale_multiply
  PORT (
    aclk : IN STD_LOGIC;
    s_axis_a_tvalid : IN STD_LOGIC;
    s_axis_a_tready : OUT STD_LOGIC;
    s_axis_a_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_b_tvalid : IN STD_LOGIC;
    s_axis_b_tready : OUT STD_LOGIC;
    s_axis_b_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axis_result_tvalid : OUT STD_LOGIC;
    m_axis_result_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;
COMPONENT adc_scale_subtract
  PORT (
    aclk : IN STD_LOGIC;
    s_axis_a_tvalid : IN STD_LOGIC;
    s_axis_a_tready : OUT STD_LOGIC;
    s_axis_a_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_b_tvalid : IN STD_LOGIC;
    s_axis_b_tready : OUT STD_LOGIC;
    s_axis_b_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m_axis_result_tvalid : OUT STD_LOGIC;
    m_axis_result_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;
signal adjusted_channel : STD_LOGIC_VECTOR(13 downto 0) := (others => '0');
signal write_enable : STD_LOGIC := '0';
signal float_data_channel : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
signal scaled_float_data_channel : STD_LOGIC_VECTOR(31 downto 0) := (others => '0');
begin

READY <= write_enable;
converter_channel: floating_point_0
          port map ( aclk => CLK,
                     aclken => write_enable,
                     s_axis_a_tvalid => '1',
                     s_axis_a_tdata => "00" & ADC_in,
                     m_axis_result_tvalid => open,
                     m_axis_result_tdata => float_data_channel);
                     
scale_channel: adc_scale_multiply
                     port map( aclk => CLK,
                               s_axis_a_tvalid => '1',
                               s_axis_a_tready => open,
                               s_axis_a_tdata => float_data_channel,
                               s_axis_b_tvalid => '1',
                               s_axis_b_tready => open,
                               s_axis_b_tdata => "00111000111111110110000000100110",--"00111000111111110110000000100110",
                               m_axis_result_tvalid => open,
                               m_axis_result_tdata => scaled_float_data_channel
                     );
--**************************************************
--subtract One -> allowing bipolar representation
-- 00111111011111110101110000101001 equals 0.9975
-- Linktipp:http://www.h-schmidt.net/FloatConverter/IEEE754de.html
--**************************************************
subtract_channel: adc_scale_subtract
                     port map( aclk =>CLK,
                               s_axis_a_tvalid => '1',
                               s_axis_a_tready => open,
                               s_axis_a_tdata => scaled_float_data_channel,
                               s_axis_b_tvalid => '1',
                               s_axis_b_tready => open,
                               s_axis_b_tdata => "00000000000000000000000000000000",--"00111111011111110101110000101001",
                               m_axis_result_tvalid => open,
                               m_axis_result_tdata => ADC_out
                     );
end Behavioral;