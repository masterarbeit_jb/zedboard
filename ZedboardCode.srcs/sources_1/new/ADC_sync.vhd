library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

-- Liest Daten vom ADC ein, wandelt sie mit dem DDR2SDR Block in Single Datarate um.
-- Anschließend findet mit dem FIFO ein wechsel des Takts statt. Statt vom ADC sind die Daten dann zum FPGA-CLK synchron. 
entity ADC_sync is
    port (
        --CLK vom Core
        GCLK : in STD_LOGIC;
        --CLK vom ADC
        clk_in_p:	in	std_logic;--	Differential	fast	clock	from	IOB
        clk_in_n:	in	std_logic;
        --RST vom ADC
        RST : in STD_LOGIC := '0';
        -- Datenbus vom ADC
       	data_in_from_pins_p	:	in	std_logic_vector(13	downto	0);
        data_in_from_pins_n	:	in	std_logic_vector(13	downto	0);
        -- Daten, synchron zu GCLK zur weiteren Verarbeitung
        ADC_A : out STD_LOGIC_VECTOR (13 downto 0);
        ADC_B : out STD_LOGIC_VECTOR (13 downto 0)
    );
end ADC_sync;

architecture Behavioral of ADC_sync is

-- Signals
signal ADC_A_odd : STD_LOGIC_VECTOR ( 6 downto 0 ) := (others => '0');
signal ADC_A_even : STD_LOGIC_VECTOR ( 6 downto 0 ) := (others => '0');
signal ADC_B_odd : STD_LOGIC_VECTOR ( 6 downto 0 ) := (others => '0');
signal ADC_B_even : STD_LOGIC_VECTOR ( 6 downto 0 ) := (others => '0');

signal ADC_A_SDR : STD_LOGIC_VECTOR ( 13 downto 0 ) := (others => '0');
signal ADC_B_SDR : STD_LOGIC_VECTOR ( 13 downto 0 ) := (others => '0');
signal sync_ADC : STD_LOGIC_VECTOR ( 27 downto 0 ) := (others => '0');

signal	ADC_SDR	:	std_logic_vector(27	downto	0)	:=	(others	=>	'0');
signal	ADC_CLK	:	STD_LOGIC	:=	'0';
-- IP-Cores
COMPONENT sync_fifo
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(27 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(27 DOWNTO 0);
    full : OUT STD_LOGIC;
    overflow : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    underflow : OUT STD_LOGIC
  );
END COMPONENT;
	component DDR2SDR
	generic
	 (-- width of the data for the system
	  SYS_W       : integer := 14;
	  -- width of the data for the device
	  DEV_W       : integer := 28);
	port
	 (
	  -- From the system into the device
	  data_in_from_pins_p     : in    std_logic_vector(SYS_W-1 downto 0);
	  data_in_from_pins_n     : in    std_logic_vector(SYS_W-1 downto 0);
	  data_in_to_device       : out   std_logic_vector(DEV_W-1 downto 0);
	
	 
	-- Clock and reset signals
	  clk_in_p                : in    std_logic;                    -- Differential fast clock from IOB
	  clk_in_n                : in    std_logic;
	  clk_out                 : out   std_logic;
	  io_reset                : in    std_logic);                   -- Reset signal for IO circuit
	end component;
BEGIN
input_convert: DDR2SDR
	port map	
	(	
		data_in_from_pins_p	=> data_in_from_pins_p,
		data_in_from_pins_n	=> data_in_from_pins_n,
		data_in_to_device	=> ADC_SDR,
		clk_in_p			=> clk_in_p,
		clk_in_n			=> clk_in_n,
		clk_out				=> ADC_CLK,
		io_reset			=> '0'
	);

rearange_ADC_SDR: process(ADC_CLK)
    begin
        if rising_edge(ADC_CLK) then
            ADC_A_SDR <= ADC_SDR(27) & ADC_SDR(13) & ADC_SDR(26) & ADC_SDR(12) & ADC_SDR(25) & ADC_SDR(11) & ADC_SDR(24) & ADC_SDR(10) & ADC_SDR(23) & ADC_SDR(9) & ADC_SDR(22) & ADC_SDR(8) & ADC_SDR(21) & ADC_SDR(7);
            ADC_B_SDR <= ADC_SDR(20) & ADC_SDR(6) & ADC_SDR(19) & ADC_SDR(5) & ADC_SDR(18) & ADC_SDR(4) & ADC_SDR(17) & ADC_SDR(3) & ADC_SDR(16) & ADC_SDR(2) & ADC_SDR(15) & ADC_SDR(1) & ADC_SDR(14) & ADC_SDR(0);
       end if;
    end process;

    
fifo : sync_fifo
    PORT MAP (
        rst => RST,
        wr_clk => ADC_CLK,
        rd_clk => GCLK,
        din => ADC_A_SDR & ADC_B_SDR,
        wr_en => '1',
        rd_en => '1',
        dout => sync_ADC,
        full => open,
        overflow => open,
        empty => open,
        underflow => open
    );
    ADC_A <= sync_ADC(27 downto 14);
    ADC_B <= sync_ADC(13 downto 0);
end Behavioral;