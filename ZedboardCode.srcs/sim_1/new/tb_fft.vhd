----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 13.04.2017 14:20:40
-- Design Name: 
-- Module Name: tb_fft - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_fft is
--  Port ( );
end tb_fft;

architecture Behavioral of tb_fft is
component fft is
      Port (
	  FFT_IM_addr	:	in	STD_LOGIC_VECTOR	(	12	downto	0	);
      FFT_IM_clk	:	in	STD_LOGIC;
      FFT_IM_din	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
      FFT_IM_dout	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
      FFT_IM_en	:	in	STD_LOGIC;
      FFT_IM_rst	:	in	STD_LOGIC;
      FFT_IM_we	:	in	STD_LOGIC_VECTOR	(	3	downto	0	);
      FFT_RE_addr	:	in	STD_LOGIC_VECTOR	(	12	downto	0	);
      FFT_RE_clk	:	in	STD_LOGIC;
      FFT_RE_din	:	in	STD_LOGIC_VECTOR	(	31	downto	0	);
      FFT_RE_dout	:	out	STD_LOGIC_VECTOR	(	31	downto	0	);
      FFT_RE_en	:	in	STD_LOGIC;
      FFT_RE_rst	:	in	STD_LOGIC;
      FFT_RE_we	:	in	STD_LOGIC_VECTOR	(	3	downto	0	);
      	
      FFT_CONFIG	:	in	std_logic_vector(15	downto 0) := (others => '0');
      
      CLK			:	in	STD_LOGIC	:=	'0';
      start		:	in	STD_LOGIC	:=	'0';
      finished	:	out	STD_LOGIC	:=	'0'
      );
    end component fft;
    -----------------------------------------------------------------------
    -- Timing constants
    -----------------------------------------------------------------------
    constant CLOCK_PERIOD : time := 10 ns;
    constant T_HOLD       : time := 1 ns;
    constant T_STROBE     : time := CLOCK_PERIOD - (1 ns);
    
    -----------------------------------------------------------------------
    -- DUT signals
    -----------------------------------------------------------------------
    
    -- General signals
    signal aclk                         : std_logic := '0';  -- the master clock
    signal start                        : std_logic := '0';
    signal finished                     : std_logic := '0';
    signal FFT_PL_PS_addr : STD_LOGIC_VECTOR ( 12 downto 0 ) := (others => '0');
    signal FFT_PL_PS_din : STD_LOGIC_VECTOR ( 63 downto 0 ) := (others => '0');
    signal FFT_PL_PS_dout : STD_LOGIC_VECTOR ( 63 downto 0 ) := (others => '0');
    signal FFT_PL_PS_en : STD_LOGIC := '0';
    signal FFT_PL_PS_rst : STD_LOGIC := '0';
    signal FFT_PL_PS_we : STD_LOGIC_VECTOR ( 7 downto 0 ) := (others => '0');
    
    signal FFT_PS_PL_addr : STD_LOGIC_VECTOR ( 12 downto 0 ) := (others => '0');
    signal FFT_PS_PL_din : STD_LOGIC_VECTOR ( 63 downto 0 ) := (others => '0');
    signal FFT_PS_PL_dout : STD_LOGIC_VECTOR ( 63 downto 0 ) := (others => '0');
    signal FFT_PS_PL_en : STD_LOGIC := '0';
    signal FFT_PS_PL_rst : STD_LOGIC := '0';
    signal FFT_PS_PL_we : STD_LOGIC_VECTOR ( 7 downto 0 ) := (others => '0');

begin
clock_gen : process
  begin
    aclk <= '0';
    wait for CLOCK_PERIOD;
    loop
      aclk <= '0';
      wait for CLOCK_PERIOD/2;
      aclk <= '1';
      wait for CLOCK_PERIOD/2;
    end loop;
end process clock_gen;

startstimn : process is
begin
	wait for CLOCK_PERIOD;
	start <= '1';
	wait for CLOCK_PERIOD*2;
	start <= '0';
	wait for 1ms;
end process startstimn;


dut: component fft
	port map(
		FFT_PL_PS_addr => FFT_PL_PS_addr,
		FFT_PL_PS_clk  => aclk,
		FFT_PL_PS_din  => FFT_PL_PS_din,
		FFT_PL_PS_dout => FFT_PL_PS_dout,
		FFT_PL_PS_en   => FFT_PL_PS_en,
		FFT_PL_PS_rst  => FFT_PL_PS_rst,
		FFT_PL_PS_we   => FFT_PL_PS_we,
		FFT_PS_PL_addr => FFT_PS_PL_addr,
		FFT_PS_PL_clk  => aclk,
		FFT_PS_PL_din  => FFT_PS_PL_din,
		FFT_PS_PL_dout => FFT_PS_PL_dout,
		FFT_PS_PL_en   => FFT_PS_PL_en,
		FFT_PS_PL_rst  => FFT_PS_PL_rst,
		FFT_PS_PL_we   => FFT_PS_PL_we,
		CLK            => aclk,
		start          => start,
		finished       => finished
	);
end Behavioral;
