----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 18.07.2017 14:19:10
-- Design Name: 
-- Module Name: CHdata_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CHdata_tb is
--  Port ( );
end CHdata_tb;

architecture Behavioral of CHdata_tb is
	component CHdata is
	Port (
		CLK : in std_logic;
		chraw : IN std_logic_vector (13 downto 0);
		data_request : in std_logic;
		data_ready : out std_logic;
		
		downsampling_div: in integer range 0 to 8192 := 0; -- Range muss weiter unten auch angepasst werden...
		
		PS_CLK : in std_logic;
		PS_addr : in std_logic_vector(12 downto 0) ;
		PS_dout : out std_logic_vector (31 downto 0)
	);
	end component CHdata;
	
	constant CLOCK_PERIOD : time := 10 ns;
	signal GCLK : std_logic := '0';
	signal ADC : unsigned(13 downto 0) := "00000000000001";
	signal data_request : std_logic := '0';
	signal data_ready : std_logic := '0';
	signal downsampling : integer range 0 to 8192;
	
	signal CH_clk : std_logic := '0';
	signal CH_addr : std_logic_vector(12 downto 0) := (others => '0');
	signal CH_dout : std_logic_vector (31 downto 0):= (others => '0');
	
begin

clock_gen : process
  begin
    GCLK <= '0';
    wait for CLOCK_PERIOD;
    loop
      GCLK <= '0';
      wait for CLOCK_PERIOD/2;
      GCLK <= '1';
      wait for CLOCK_PERIOD/2;
    end loop;
end process clock_gen;

data_requestp : process
  begin
    wait for CLOCK_PERIOD*4;
    data_request <= '1';
    wait for CLOCK_PERIOD*2;
    data_request <= '0';
    wait;
end process data_requestp;


dut : component CHdata
port map (
	CLK				=> GCLK,
	chraw			=> std_logic_vector(ADC),
	data_request	=> data_request,
	data_ready		=> data_ready,
	
	downsampling_div=> 20,
	
	PS_CLK			=> CH_clk,
	PS_addr			=> CH_addr,
	PS_dout			=> CH_dout
);
end Behavioral;
