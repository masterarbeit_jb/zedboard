----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 21.03.2017 15:47:54
-- Design Name: 
-- Module Name: tb_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_top is
--  Port ( );
end tb_top;

architecture Behavioral of tb_top is
component top is
	Port (
		GCLK : in std_logic;
		SW : in std_logic_vector (7 downto 0);
		LED : out std_logic_vector (7 downto 0);
		
		FMC_INPUT_A_P : in std_logic_vector(6 downto 0);
		FMC_INPUT_A_N : in std_logic_vector(6 downto 0);
		FMC_INPUT_B_P : in std_logic_vector(6 downto 0);
		FMC_INPUT_B_N : in std_logic_vector(6 downto 0);
		
		FMC_INPUT_CLK_P : in std_logic;
		FMC_INPUT_CLK_N : in std_logic;
		
		MUX_S1_A : out std_logic_vector(3 downto 0);
		MUX_S2_A : out std_logic_vector(3 downto 0);
		MUX_E1_A : out std_logic_vector(3 downto 0);
		MUX_E2_A : out std_logic_vector(3 downto 0);
		MUX_V_A : out std_logic_vector(3 downto 0);
		
		MUX_S1_EN : out std_logic;
		MUX_S2_EN : out std_logic;
		MUX_E1_EN : out std_logic;
		MUX_E2_EN : out std_logic;
		MUX_V_EN : out std_logic;
	
		DDR_cas_n : inout STD_LOGIC;
		DDR_cke : inout STD_LOGIC;
		DDR_ck_n : inout STD_LOGIC;
		DDR_ck_p : inout STD_LOGIC;
		DDR_cs_n : inout STD_LOGIC;
		DDR_reset_n : inout STD_LOGIC;
		DDR_odt : inout STD_LOGIC;
		DDR_ras_n : inout STD_LOGIC;
		DDR_we_n : inout STD_LOGIC;
		DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
		DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
		DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
		DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
		
		FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
		FIXED_IO_ddr_vrn : inout STD_LOGIC;
		FIXED_IO_ddr_vrp : inout STD_LOGIC;
		FIXED_IO_ps_srstb : inout STD_LOGIC;
		FIXED_IO_ps_clk : inout STD_LOGIC;
		FIXED_IO_ps_porb : inout STD_LOGIC
	);
end component top;
	signal GCLK :std_logic := '0';
	signal SW : std_logic_vector(7 downto 0) := (others => 0);
	signal LED : std_logic_vector(7 downto 0) := (others => 0);
	
begin
unit: component top
	port map(
		GCLK              => GCLK,
		SW                => SW,
		LED               => LED,
		FMC_INPUT_A_P     => open,
		FMC_INPUT_A_N     => open,
		FMC_INPUT_B_P     => open,
		FMC_INPUT_B_N     => open,
		FMC_INPUT_CLK_P   => open,
		FMC_INPUT_CLK_N   => open,
		MUX_S1_A          => open,
		MUX_S2_A          => open,
		MUX_E1_A          => open,
		MUX_E2_A          => open,
		MUX_V_A           => open,
		MUX_S1_EN         => open,
		MUX_S2_EN         => open,
		MUX_E1_EN         => open,
		MUX_E2_EN         => open,
		MUX_V_EN          => open,
		DDR_cas_n         => open,
		DDR_cke           => open,
		DDR_ck_n          => open,
		DDR_ck_p          => open,
		DDR_cs_n          => open,
		DDR_reset_n       => open,
		DDR_odt           => open,
		DDR_ras_n         => open,
		DDR_we_n          => open,
		DDR_ba            => open,
		DDR_addr          => open,
		DDR_dm            => open,
		DDR_dq            => open,
		DDR_dqs_n         => open,
		DDR_dqs_p         => open,
		FIXED_IO_mio      => open,
		FIXED_IO_ddr_vrn  => open,
		FIXED_IO_ddr_vrp  => open,
		FIXED_IO_ps_srstb => open,
		FIXED_IO_ps_clk   => open,
		FIXED_IO_ps_porb  => open
	);

end Behavioral;
