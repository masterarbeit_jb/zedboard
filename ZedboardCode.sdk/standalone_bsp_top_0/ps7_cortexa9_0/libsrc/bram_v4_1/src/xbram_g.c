
/*******************************************************************
*
* CAUTION: This file is automatically generated by HSI.
* Version: 
* DO NOT EDIT.
*
* Copyright (C) 2010-2017 Xilinx, Inc. All Rights Reserved.*
*Permission is hereby granted, free of charge, to any person obtaining a copy
*of this software and associated documentation files (the Software), to deal
*in the Software without restriction, including without limitation the rights
*to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*copies of the Software, and to permit persons to whom the Software is
*furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in
*all copies or substantial portions of the Software.
* 
* Use of the Software is limited solely to applications:
*(a) running on a Xilinx device, or
*(b) that interact with a Xilinx device through a bus or interconnect.
*
*THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
*XILINX BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
*WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
*OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*Except as contained in this notice, the name of the Xilinx shall not be used
*in advertising or otherwise to promote the sale, use or other dealings in
*this Software without prior written authorization from Xilinx.
*

* 
* Description: Driver configuration
*
*******************************************************************/

#include "xparameters.h"
#include "xbram.h"

/*
* The configuration table for devices
*/

XBram_Config XBram_ConfigTable[] =
{
	{
		XPAR_BRAM_CH_A_DEVICE_ID,
		XPAR_BRAM_CH_A_DATA_WIDTH,
		XPAR_BRAM_CH_A_ECC,
		XPAR_BRAM_CH_A_FAULT_INJECT,
		XPAR_BRAM_CH_A_CE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_CH_A_UE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_CH_A_ECC_STATUS_REGISTERS,
		XPAR_BRAM_CH_A_CE_COUNTER_WIDTH,
		XPAR_BRAM_CH_A_ECC_ONOFF_REGISTER,
		XPAR_BRAM_CH_A_ECC_ONOFF_RESET_VALUE,
		XPAR_BRAM_CH_A_WRITE_ACCESS,
		XPAR_BRAM_CH_A_S_AXI_BASEADDR,
		XPAR_BRAM_CH_A_S_AXI_HIGHADDR,
		XPAR_BRAM_CH_A_S_AXI_CTRL_BASEADDR,
		XPAR_BRAM_CH_A_S_AXI_CTRL_HIGHADDR
	},
	{
		XPAR_BRAM_CH_B_DEVICE_ID,
		XPAR_BRAM_CH_B_DATA_WIDTH,
		XPAR_BRAM_CH_B_ECC,
		XPAR_BRAM_CH_B_FAULT_INJECT,
		XPAR_BRAM_CH_B_CE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_CH_B_UE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_CH_B_ECC_STATUS_REGISTERS,
		XPAR_BRAM_CH_B_CE_COUNTER_WIDTH,
		XPAR_BRAM_CH_B_ECC_ONOFF_REGISTER,
		XPAR_BRAM_CH_B_ECC_ONOFF_RESET_VALUE,
		XPAR_BRAM_CH_B_WRITE_ACCESS,
		XPAR_BRAM_CH_B_S_AXI_BASEADDR,
		XPAR_BRAM_CH_B_S_AXI_HIGHADDR,
		XPAR_BRAM_CH_B_S_AXI_CTRL_BASEADDR,
		XPAR_BRAM_CH_B_S_AXI_CTRL_HIGHADDR
	},
	{
		XPAR_BRAM_FFT_IM_DEVICE_ID,
		XPAR_BRAM_FFT_IM_DATA_WIDTH,
		XPAR_BRAM_FFT_IM_ECC,
		XPAR_BRAM_FFT_IM_FAULT_INJECT,
		XPAR_BRAM_FFT_IM_CE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_FFT_IM_UE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_FFT_IM_ECC_STATUS_REGISTERS,
		XPAR_BRAM_FFT_IM_CE_COUNTER_WIDTH,
		XPAR_BRAM_FFT_IM_ECC_ONOFF_REGISTER,
		XPAR_BRAM_FFT_IM_ECC_ONOFF_RESET_VALUE,
		XPAR_BRAM_FFT_IM_WRITE_ACCESS,
		XPAR_BRAM_FFT_IM_S_AXI_BASEADDR,
		XPAR_BRAM_FFT_IM_S_AXI_HIGHADDR,
		XPAR_BRAM_FFT_IM_S_AXI_CTRL_BASEADDR,
		XPAR_BRAM_FFT_IM_S_AXI_CTRL_HIGHADDR
	},
	{
		XPAR_BRAM_FFT_RE_DEVICE_ID,
		XPAR_BRAM_FFT_RE_DATA_WIDTH,
		XPAR_BRAM_FFT_RE_ECC,
		XPAR_BRAM_FFT_RE_FAULT_INJECT,
		XPAR_BRAM_FFT_RE_CE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_FFT_RE_UE_FAILING_REGISTERS,
		0,
		XPAR_BRAM_FFT_RE_ECC_STATUS_REGISTERS,
		XPAR_BRAM_FFT_RE_CE_COUNTER_WIDTH,
		XPAR_BRAM_FFT_RE_ECC_ONOFF_REGISTER,
		XPAR_BRAM_FFT_RE_ECC_ONOFF_RESET_VALUE,
		XPAR_BRAM_FFT_RE_WRITE_ACCESS,
		XPAR_BRAM_FFT_RE_S_AXI_BASEADDR,
		XPAR_BRAM_FFT_RE_S_AXI_HIGHADDR,
		XPAR_BRAM_FFT_RE_S_AXI_CTRL_BASEADDR,
		XPAR_BRAM_FFT_RE_S_AXI_CTRL_HIGHADDR
	}
};


