/*
 * uart_matlab_functions.c
 *
 *  Created on: 05.04.2017
 *      Author: BioPerzept
 */

#include <stdio.h>
#include "platform.h"
#include "uart_matlab.h"
#include "platform_config.h"

#include "sleep.h"
#include "xuartps.h"

void uart_matlab_send_float_array(float* data, int size) {
	for (int i = 0; i <size; i++)
	{
		uart_matlab_send_float(data[i]);
	}
}
void uart_matlab_send_float(float number) {
	uni_f_i tosend;
	tosend.f = number;

	char b;
	b = (char) tosend.i;
	outbyte(b);
	b = (char) (tosend.i >> 8);
	outbyte(b);
	b = (char) (tosend.i >> 16);
	outbyte(b);
	b = (char) (tosend.i >> 24);
	outbyte(b);
}
float uart_matlab_get_float(void)
{
	while (!XUartPs_IsReceiveData(STDIN_BASEADDRESS));

	uni_f_i input;
	input.i = inbyte();
	input.i = (((int) inbyte()) << 8) + input.i;
	input.i = (((int) inbyte()) << 16) + input.i;
	input.i = (((int) inbyte()) << 24) + input.i;
	return input.f;
}
void uart_matlab_get_float_array(float data[], int size) {
	for(int i = 0; i<size; i++)
	{
		data[i] = uart_matlab_get_float();
	}
}
int uart_matlab_read2digits(int last) {
	int amp = last;
	// Sleep for a 1ms
	usleep(1000);
	//Check for 1. Char
	if(XUartPs_IsReceiveData(STDIN_BASEADDRESS))
	{
		amp = (inbyte()-48)*10;
		// Check for 2. Byte
		if(XUartPs_IsReceiveData(STDIN_BASEADDRESS))
		{
			amp += (inbyte()-48);
		}
		else
		{
			amp = last;
		}
	}
	else
	{
		amp = last;
	}
	return amp;
}
int uart_matlab_read4digits(int last) {
	int amp = last;
	// Sleep for a 1ms
	usleep(1000);
	//Check for Byte aviable
	if(XUartPs_IsReceiveData(STDIN_BASEADDRESS))
	{
		amp = (inbyte()-48)*1000;
		amp += (inbyte()-48)*100;
		amp += (inbyte()-48)*10;
		amp += (inbyte()-48);
	}
	else
	{
		amp = last;
	}
	return amp;
}
