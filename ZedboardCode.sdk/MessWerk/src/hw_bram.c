/*
 * hw_bram.c
 *
 *  Created on: 19.06.2017
 *      Author: BioPerzept
 */
#include "platform_config.h"


float BRAM_readfloat( int MemBaseAddress, int RegisterOffset)
{
	static uni_f_i temp;

	temp.i = XBram_ReadReg(MemBaseAddress, RegisterOffset);
	return temp.f;
}

void BRAM_readfloat_array( int MemBaseAddress, float array[], int size)
{
	for (int i=0; i<size; i++)
	{
		array[i] = BRAM_readfloat(MemBaseAddress, i*4);
	}
}

void BRAM_writefloat( int MemBaseAddress, int RegisterOffset, float value)
{
	static uni_f_i temp;
	temp.f = value;
	XBram_WriteReg(MemBaseAddress, RegisterOffset, temp.i);
}
void BRAM_writefloat_array( int MemBaseAddress, float values[], int size)
{
	for (int i = 0;i<size;i++)
	{
		BRAM_writefloat(MemBaseAddress, i*4, values[i]);
	}
}
