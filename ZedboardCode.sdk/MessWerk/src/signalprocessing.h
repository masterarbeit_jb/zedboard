/*
 * messurement.h
 *
 *  Created on: 05.07.2017
 *      Author: BioPerzept
 */

#ifndef SRC_SIGNALPROCESSING_H_
#define SRC_SIGNALPROCESSING_H_
#include "platform_config.h"

void signal_process_all(chan_data *CH_A, chan_data *CH_B, int size, int DS);

#endif /* SRC_SIGNALPROCESSING_H_ */
