#include "signalprocessing.h"

#include "hw_bram.h"
#include "math.h"
#include "windows.h"
#include "hw_fft.h"

float rms_bb(float *input, int size);
void betrag_quadrat(float fft_betrag[], float fft_re[], float fft_im[], int size);
float rms_sb(float *fft_betrag, int size);
int search_peak(float fft_betrag[],int size);
float getAmplitude(float *fft_betrag, int index);
void akf(float input[], float output[], int size);
float zerros[2048] = {0};

void signal_process_all(chan_data *CH_A, chan_data *CH_B, int size, int DS)
{
	// Effektivwert berechnen

	CH_A->rms_bb = rms_bb(CH_A->raw, size);
	CH_B->rms_bb = rms_bb(CH_B->raw, size);
	//Fenster anwenden

	windowing(CH_A->raw, CH_A->win, flattopwin, size);
	windowing(CH_B->raw, CH_B->win, flattopwin, size);

	// FFT (ohne Fenster) durchführen
	//hwfft(CH_A->raw, zerros, CH_A->fft_re, CH_A->fft_im, size, 1);
	//hwfft(CH_B->raw, zerros, CH_B->fft_re, CH_B->fft_im, size, 1);

	// FFT (mit Fenster) durchführen
	hwfft(CH_A->win, zerros, CH_A->fft_re, CH_A->fft_im, size, 1);
	hwfft(CH_B->win, zerros, CH_B->fft_re, CH_B->fft_im, size, 1);


	// Betragsquadrat der FFT ermitteln
	betrag_quadrat(CH_A->fft_betrag_quadrat, CH_A->fft_re, CH_A->fft_im, size);
	betrag_quadrat(CH_B->fft_betrag_quadrat, CH_B->fft_re, CH_B->fft_im, size);
	// Search Peak
	CH_A->peak_index = search_peak(CH_A->fft_betrag_quadrat, size);
	CH_B->peak_index = search_peak(CH_B->fft_betrag_quadrat, size);
	// Bestimme Frequenz
	CH_A->f_peak = CH_A->peak_index * (ADC_SR/size)/(2^DS);
	CH_B->f_peak = CH_B->peak_index * (ADC_SR/size)/(2^DS);
	// Schmallband RMS bestimmen
	CH_A->rms_sb = getAmplitude(CH_A->fft_betrag_quadrat, CH_A->peak_index)/M_SQRT2;
	CH_B->rms_sb = getAmplitude(CH_B->fft_betrag_quadrat, CH_B->peak_index)/M_SQRT2;

	// AKF durchführen
	//akf(CH_A->raw, CH_A->akf, size);
	//akf(CH_B->raw, CH_B->akf, size);

}
float rms_bb(float *input, int size)
{
	float rms = 0.0;
	for(int i=0; i<size; i++) {
		rms+= pow(input[i],2);
	}
	return sqrt(rms/size);
}
void betrag_quadrat(float fft_betrag[], float fft_re[], float fft_im[], int size)
{
	for(int i=0; i<size; i++) {
		fft_betrag[i] = (pow(fft_re[i], 2)+ pow( fft_im[i], 2));
	}
}
int search_peak(float fft_betrag[],int size)
{
	int max_index = 0;
	float_t max = 0;
	for(int i=0; i<size/2; i++) {
		if(fft_betrag[i]>max) {
			max = fft_betrag[i];
			max_index = i;
		}
	}
	return max_index;
}
float getAmplitude(float *fft_betrag, int index)
{
	return sqrt(fft_betrag[index]);
}
void akf(float input[], float output[], int size)
{
	float fft_re [2048];
	float fft_im [2048];
	float Cr [2048];
	hwfft(input, zerros, fft_re, fft_im, size, 1);
	for(int i=0; i<size; i++) {
		Cr[i] = (pow(fft_re[i], 2)+pow( fft_im[i], 2));
	}
	hwfft(Cr, zerros, output, fft_im, size, 0);

}
