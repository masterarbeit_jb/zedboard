#ifndef __PLATFORM_CONFIG_H_
#define __PLATFORM_CONFIG_H_

#define STDOUT_IS_PS7_UART
#define UART_DEVICE_ID 0


#include "xgpiops.h" 			// For the GPIO @ Processorsystem
extern XGpioPs GpioPs; 			// Name of the driver instance for GPIO Device on Ps
#define LD9 		7			// Pin Number of LD 9 (between OLED and double buttons)

#include "xgpio.h"				// For the GPIO over AXI
extern XGpio PS_PL0;				// AXI-GPIO PS to PL
extern XGpio PS_PL1;				// AXI-GPIO PL to PS 1
extern XGpio PL_PS;				// AXI-GPIO PL to PS
#define CH_PS_PL_0 		1
#define CH_PS_PL_1		2
#define CH_PS_PL_2		1
#define CH_PS_PL_3		2
#define CH_PL_PS_0 		1
#define CH_PL_PS_1		2
#define ADC_SR			100e6


#include "xbram.h"
#include "xbram_hw.h"
extern XBram BRAM_CH_A;
extern XBram BRAM_CH_B;
extern XBram BRAM_FFT_RE;
extern XBram BRAM_FFT_IM;
extern XBram_Config *BRAM_Config_CH_A;
extern XBram_Config *BRAM_Config_CH_B;
extern XBram_Config *BRAM_Config_FFT_RE;
extern XBram_Config *BRAM_Config_FFT_IM;


// Typedef for PS <->PL communication
typedef union {
    struct {
    	unsigned int MUX_S1_A 		: 4;	// 0-3
    	unsigned int MUX_S2_A 		: 4;	// 4-7
    	unsigned int MUX_E2_A 		: 4;	// 8-11
    	unsigned int MUX_E1_A 		: 4;	// 12-15
    	unsigned int MUX_V_A 		: 4;	// 16-19
    	unsigned int MUX_S1_EN 		: 1;	// 20
    	unsigned int MUX_S2_EN 		: 1;	// 21
    	unsigned int MUX_E2_EN 		: 1;	// 22
    	unsigned int MUX_E1_EN 		: 1;	// 23
    	unsigned int MUX_V_EN 		: 1;	// 24
    	unsigned int oversampling 	: 4;	// 25-28
    	unsigned int data_request	: 1;	// 29
    	unsigned int fft_start		: 1;	// 30
    } s;
    uint32_t asInt;
} PS_PL_0t;

typedef union {
	struct {
		unsigned int FWD	: 1;	// 0. Bit, wenn 1 -> Vorw�rts
		unsigned int SCALE 	: 12;
		unsigned int unused	: 11;
	}s;
	int FFT_CONFIGasInt;  	//8-31
} FFT_CONFIGt;		//8-31

typedef union {
    struct {
    	unsigned int LED 			: 8;	//0-7
    	unsigned int FFT_FWD		: 1;
    	unsigned int FFT_SCALE		: 12;
    	unsigned int FFT_unused		: 3;
    } s;
    uint32_t asInt;
} PS_PL_1t;
typedef union {
    struct {
    	unsigned int decimation		: 16;	//0-16
    } s;
    uint32_t asInt;
} PS_PL_2t;

typedef union {
    struct {
    	unsigned int SW 			: 8;	// 0-7
    	unsigned int data_ready 	: 1;	// 8
    	unsigned int FFT_ready		: 1;	// 9
    } s;
    uint32_t asInt;
} PL_PS_0t;

typedef union {
    struct {
    } s;
    uint32_t asInt;
} PL_PS_1t;

typedef union {
	int32_t i;
	float f;
} uni_f_i;

typedef union {
	int64_t i;
	struct {
		float re;
		float im;
	} cplx;
} uni_cplx_i;

typedef struct {
	float raw[2048];
	float win[2048];
	float fft_re[2048];
	float fft_im[2048];
	float fft_betrag_quadrat[2048];
	float akf[2048];
	float peak_index;
	float rms_bb;
	float rms_sb;
	float f_peak;
}chan_data;

typedef struct {
	int state;
	int s1;
	int s2;
	int e1;
	int e2;
}messung;

extern PL_PS_0t PL_PS_0;
extern PL_PS_1t PL_PS_1;
extern PS_PL_0t PS_PL_0;
extern PS_PL_1t PS_PL_1;
extern PS_PL_2t PS_PL_2;

#endif
