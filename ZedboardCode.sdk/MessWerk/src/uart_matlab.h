/*
 * uart_matlab_functions.h
 *
 *  Created on: 05.04.2017
 *      Author: BioPerzept
 */

#ifndef SRC_UART_MATLAB_H_
#define SRC_UART_MATLAB_H_


#define MATLAB_CMD_CH_A_RAW 			'a'		// Rohdaten Sensor
#define MATLAB_CMD_CH_B_RAW 			'b'		// Rohdaten Emitter
#define MATLAB_CMD_SET_MUX_V			'c'		// Verst�rkungsstufe Sensor
#define MATLAB_CMD_S1_ADR				'd'		// Adresse MUX 1 (Sensor 1)
#define MATLAB_CMD_S2_ADR				'e'		// Adresse MUX 2 (Sensor 2)
#define MATLAB_CMD_E1_ADR				'f'		// Adresse MUX 3 (Emitter 1)
#define MATLAB_CMD_E2_ADR				'g'		// Adresse MUX 4 (Emitter 2)
#define MATLAB_CMD_CH_A_BB_RMS			'h'		// Klassischer Effektivwert Sensor
#define MATLAB_CMD_CH_B_BB_RMS			'i'		// Klassischer Effektivwert Emitter
#define MATLAB_CMD_CH_A_SB_RMS			'j'		// Schmalband Effektivwert Sensor
#define MATLAB_CMD_CH_B_SB_RMS			'k'		// Schmalband Effektivwert Emitter
#define MATLAB_CMD_CH_A_FFT				'l'		// FFT Sensor
#define MATLAB_CMD_CH_B_FFT				'm'		// FFT Emitter
#define MATLAB_CMD_NEW_MESSURE			'n'		// Neue Messung starten
#define MATLAB_CMD_NEW_SIM				'o'		// Neue Simulation starten
#define MATLAB_CMD_CALC					'p'		// Berechnen (Veraltet)
#define MATLAB_CMD_MULITPLY_FLATTOP 	'q'		// Flattop-Fenster berechnen (Veraltet)
#define MATLAB_CMD_DOWNSAMPLING			'r'		// Downsampling einstellen
#define MATLAB_CMD_GET_MUX_V			's'		// Verst�rkungsstufe Sensor abfragen
#define MATLAB_CMD_NEW_MESSURE_AUTO_AMP 't'		// Neue Messung mit automatischer Skalierung (Testphase)
#define MATLAB_CMD_CH_A_AKF				'u'		// Berechnung der AKF des Sensors (nicht implementiert)
#define MATLAB_CMD_CH_B_AKF				'v'		// Berechnung der AKF des Emitters (nicht implementiert)
#define MATLAB_CMD_CH_A_F_PEAK			'w'		// R�ckgabe der Spitzenfrequenz des Sensorsignals
#define MATLAB_CMD_CH_B_F_PEAK			'x'		// R�ckgabe der Spitzenfrequenz des Emittersignals


void uart_matlab_send_float_array(float* data, int size);

int uart_matlab_read2digits(int last);
int uart_matlab_read4digits(int last);
void uart_matlab_send_float(float number);
float uart_matlab_get_float(void);

#endif /* SRC_UART_MATLAB_H_ */
