#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "sleep.h"
#include "math.h"
#include "xuartps.h"

#include "xdebug.h"
#include "xparameters.h"

#include "uart_matlab.h"
#include "hw_fft.h"
#include "hw_bram.h"
#include "windows.h"
#include "signalprocessing.h"
/*
 * Device instance definitions
 */


#if defined(XPAR_UARTNS550_0_BASEADDR)
#include "xuartns550_l.h"       /* to use uartns550 */
#endif


#define DATABLOCKSIZE 2048

XGpioPs GpioPs;						// Name of the driver instance for GPIO Device on PS
XGpio PL_PS;						// AXI-GPIO PL to PS
XGpio PS_PL0;						// AXI-GPIO PS to PL
XGpio PS_PL1;						// AXI-GPIO PS to PL 1
XBram BRAM_CH_A;
XBram BRAM_CH_B;
XBram BRAM_FFT_RE;
XBram BRAM_FFT_IM;
XBram_Config *BRAM_Config_CH_A;
XBram_Config *BRAM_Config_CH_B;
XBram_Config *BRAM_Config_FFT_RE;
XBram_Config *BRAM_Config_FFT_IM;

uni_f_i FFT_result_re[DATABLOCKSIZE];
uni_f_i FFT_result_im[DATABLOCKSIZE];

PS_PL_0t PS_PL_0 = { .asInt = 0 };
PS_PL_1t PS_PL_1 = { .asInt = 0 };
PS_PL_2t PS_PL_2 = { .asInt = 0 };

PL_PS_0t PL_PS_0 = { .asInt = 0 };
PL_PS_1t PL_PS_1 = { .asInt = 0 };


int main()
{
	//Initalisierung

	init_platform();
	// MUX INIT
	PS_PL_0.s.MUX_E1_EN = 1;
	PS_PL_0.s.MUX_E2_EN = 1;
	PS_PL_0.s.MUX_S1_EN = 1;
	PS_PL_0.s.MUX_S2_EN = 1;

	PS_PL_0.s.MUX_V_EN 	= 1;
	PS_PL_0.s.MUX_V_A	= 1;
	PS_PL_0.s.MUX_E1_A = 0;
	PS_PL_0.s.MUX_E2_A = 1;
	PS_PL_0.s.MUX_S1_A = 0;
	PS_PL_0.s.MUX_S2_A = 1;
	PS_PL_0.s.oversampling = 0;

	PS_PL_1.s.LED = 0x0;

	chan_data CH_A;
	chan_data CH_B;

	//Matlab Befehl
	char command = '\0';

	int i = 0;
	//Tempor�re Variable, nur f�r einen Schleifendurchlauf g�ltig.
	int glow_PWM = 0;
	int glow_TRIGGER = 1;
	int glow_TOGGLE = 1;
	PS_PL_2.s.decimation = 4;
	PS_PL_1.asInt = 0;

	XGpioPs_WritePin(&GpioPs, LD9, 0x00);
    while(1) {
    	//
    	glow_PWM++;
    	if (glow_PWM > 500) {
    		glow_PWM = 0;
    		glow_TRIGGER += glow_TOGGLE;
    		(glow_TRIGGER >500 || glow_TRIGGER < 0) ? glow_TOGGLE *= -1:1;
     	}
   		XGpioPs_WritePin(&GpioPs, LD9, (glow_PWM < glow_TRIGGER));
   		//Read & Write AXI GPIO
    	PL_PS_0.asInt = XGpio_DiscreteRead (&PL_PS, CH_PL_PS_0);	//Read from PL0
    	PL_PS_1.asInt = XGpio_DiscreteRead (&PL_PS, CH_PL_PS_1);	//Read from PL1
    	XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
    	XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_1, PS_PL_1.asInt);
    	XGpio_DiscreteWrite(&PS_PL1, CH_PS_PL_2, PS_PL_2.asInt);

		// Communication State machine
    	if (XUartPs_IsReceiveData(STDIN_BASEADDRESS))
    	{
    		command = inbyte();
    		//Hilfsvariablen f�r die Verst�rkung;
			float min = MAXFLOAT;
			float max = -MAXFLOAT;
    		switch (command) {
    			case MATLAB_CMD_CH_A_RAW:
					//uart_matlab_send_float_array(CH_A.raw, DATABLOCKSIZE);
					uart_matlab_send_float_array(CH_A.raw, DATABLOCKSIZE);
					break;
				case MATLAB_CMD_CH_B_RAW:
					uart_matlab_send_float_array(CH_B.raw, DATABLOCKSIZE);
					break;
				case MATLAB_CMD_SET_MUX_V:
					PS_PL_0.s.MUX_V_A = uart_matlab_read2digits(PS_PL_0.s.MUX_V_A);
					break;
				case MATLAB_CMD_GET_MUX_V:
					uart_matlab_send_float(PS_PL_0.s.MUX_V_A);
					break;
				case MATLAB_CMD_DOWNSAMPLING:
					PS_PL_2.s.decimation = uart_matlab_read4digits(PS_PL_0.s.oversampling);
					break;
				case MATLAB_CMD_S1_ADR:
					PS_PL_0.s.MUX_S1_A = uart_matlab_read2digits(PS_PL_0.s.MUX_S1_A);
					break;
				case MATLAB_CMD_S2_ADR:
					PS_PL_0.s.MUX_S2_A = uart_matlab_read2digits(PS_PL_0.s.MUX_S2_A);
					break;
				case MATLAB_CMD_E1_ADR:
					PS_PL_0.s.MUX_E1_A = uart_matlab_read2digits(PS_PL_0.s.MUX_E1_A);
					break;
				case MATLAB_CMD_E2_ADR:
					PS_PL_0.s.MUX_E2_A = uart_matlab_read2digits(PS_PL_0.s.MUX_E2_A);
					break;
				case MATLAB_CMD_CH_A_BB_RMS:
					uart_matlab_send_float(CH_A.rms_bb);
					break;
				case MATLAB_CMD_CH_B_BB_RMS:
					uart_matlab_send_float(CH_B.rms_bb);
					break;
				case MATLAB_CMD_CH_A_SB_RMS:
					uart_matlab_send_float(CH_A.rms_sb);
					break;
				case MATLAB_CMD_CH_B_SB_RMS:
					uart_matlab_send_float(CH_B.rms_sb);
					break;
				case MATLAB_CMD_CH_A_FFT:
					uart_matlab_send_float_array(CH_A.fft_re, DATABLOCKSIZE);
					uart_matlab_send_float_array(CH_A.fft_im, DATABLOCKSIZE);
					break;
				case MATLAB_CMD_CH_B_FFT:
					uart_matlab_send_float_array(CH_B.fft_re, DATABLOCKSIZE);
					uart_matlab_send_float_array(CH_B.fft_im, DATABLOCKSIZE);
					break;
				case MATLAB_CMD_CH_A_AKF:
					uart_matlab_send_float_array(CH_B.akf, DATABLOCKSIZE);
					break;
				case MATLAB_CMD_CH_B_AKF:
					uart_matlab_send_float_array(CH_B.akf, DATABLOCKSIZE);
					break;
				case MATLAB_CMD_CH_A_F_PEAK:
					uart_matlab_send_float(CH_A.f_peak);
					break;
				case MATLAB_CMD_CH_B_F_PEAK:
					uart_matlab_send_float(CH_B.f_peak);
					break;
				case MATLAB_CMD_NEW_MESSURE:
					//Fordere Messung an
					PS_PL_0.s.data_request = 1;
					XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
					PS_PL_0.s.data_request = 0;
					XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
					do {
						PL_PS_0.asInt = XGpio_DiscreteRead(&PL_PS, CH_PL_PS_0);
					}while(PL_PS_0.s.data_ready == 0);
					// Messung vom AD-Wandler auslesen
					BRAM_readfloat_array(BRAM_Config_CH_A->MemBaseAddress, CH_A.raw, DATABLOCKSIZE);
					BRAM_readfloat_array(BRAM_Config_CH_B->MemBaseAddress, CH_B.raw, DATABLOCKSIZE);

					// Signalverarbeitung durchf�hren
					signal_process_all(&CH_A, &CH_B, DATABLOCKSIZE, PS_PL_2.s.decimation);
					break;
				case MATLAB_CMD_NEW_MESSURE_AUTO_AMP:
					// Verst�rkung einstellen
					PS_PL_0.s.MUX_V_A = 15;
					do {
						XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
						usleep(10000);
						//Fordere Messung an
						PS_PL_0.s.data_request = 1;
						XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
						PS_PL_0.s.data_request = 0;
						XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
						usleep(1000);
						do {
							PL_PS_0.asInt = XGpio_DiscreteRead(&PL_PS, CH_PL_PS_0);
						}while(PL_PS_0.s.data_ready == 0);
						// Save Messurment
						BRAM_readfloat_array(BRAM_Config_CH_A->MemBaseAddress, CH_A.raw, DATABLOCKSIZE);
						BRAM_readfloat_array(BRAM_Config_CH_B->MemBaseAddress, CH_B.raw, DATABLOCKSIZE);
						//Search min/max of CH A
						min = MAXFLOAT;
						max = -MAXFLOAT;
						for (int i = 0; i < DATABLOCKSIZE; i++)
						{
							if (CH_A.raw[i] < min)
							{
								min = CH_A.raw[i];
							}
							if (CH_A.raw[i] > max)
							{
								max = CH_A.raw[i];
							}
						}
						PS_PL_0.s.MUX_V_A--;					// Inkrementieren f�r den n�chsten Schleifendurchlauf
					} while (max >= 0.95 || min <= -0.95);
					PS_PL_0.s.MUX_V_A++;					// Letzte inkrementieren r�ckg�ngig machen;
					break;
				case MATLAB_CMD_NEW_SIM:
					for(i = 0; i<DATABLOCKSIZE; i++)
					{
						CH_A.raw[i] = uart_matlab_get_float();
					}
					for(i = 0; i<DATABLOCKSIZE; i++)
					{
						CH_B.raw[i] = uart_matlab_get_float();
					}
					break;
				case '\n':
				case '\r':
					break;
				default:
					break;
			}
    	}
    }

    cleanup_platform();
    return 0;
}
