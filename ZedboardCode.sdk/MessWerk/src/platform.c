/******************************************************************************
*
* Copyright (C) 2010 - 2015 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

#include "xparameters.h"
#include "xil_cache.h"

#include "platform_config.h"

/*
 * Uncomment one of the following two lines, depending on the target,
 * if ps7/psu init source files are added in the source directory for
 * compiling example outside of SDK.
 */
/*#include "ps7_init.h"*/
/*#include "psu_init.h"*/

#ifdef STDOUT_IS_16550
 #include "xuartns550_l.h"

 #define UART_BAUD 9600
#endif

void
enable_caches()
{
#ifdef __PPC__
    Xil_ICacheEnableRegion(CACHEABLE_REGION_MASK);
    Xil_DCacheEnableRegion(CACHEABLE_REGION_MASK);
#elif __MICROBLAZE__
#ifdef XPAR_MICROBLAZE_USE_ICACHE
    Xil_ICacheEnable();
#endif
#ifdef XPAR_MICROBLAZE_USE_DCACHE
    Xil_DCacheEnable();
#endif
#endif
}

void
disable_caches()
{
    Xil_DCacheDisable();
    Xil_ICacheDisable();
}

void
init_uart()
{
#ifdef STDOUT_IS_16550
    XUartNs550_SetBaud(STDOUT_BASEADDR, XPAR_XUARTNS550_CLOCK_HZ, UART_BAUD);
    XUartNs550_SetLineControlReg(STDOUT_BASEADDR, XUN_LCR_8_DATA_BITS);
#endif
    /* Bootrom/BSP configures PS7/PSU UART to 115200 bps */
}

void
init_platform()
{
    /*
     * If you want to run this example outside of SDK,
     * uncomment one of the following two lines and also #include "ps7_init.h"
     * or #include "ps7_init.h" at the top, depending on the target.
     * Make sure that the ps7/psu_init.c and ps7/psu_init.h files are included
     * along with this example source files for compilation.
     */
    /* ps7_init();*/
    /* psu_init();*/
    enable_caches();
    init_uart();

    int Status;

    /*
     * Setup for LD 9
     */
    XGpioPs_Config *ConfigPtr;													// prepare config pointer
	ConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);				// Lookup config
	Status = XGpioPs_CfgInitialize(&GpioPs, ConfigPtr, ConfigPtr->BaseAddr);	// Init  GPIO
    XGpioPs_SetDirectionPin(&GpioPs, LD9, 1);									// set pin of LD9 as an output
    XGpioPs_SetOutputEnablePin(&GpioPs, LD9, 1);								// enable output pin of LD9
    XGpioPs_WritePin(&GpioPs, LD9, 0x1);										// Switch LD9 on


    /*
     * Setup for PS to PL 0
     */
	XGpio_Config *GPIO_PS_PL0_config;											// prepare config pointer
	GPIO_PS_PL0_config = XGpio_LookupConfig(XPAR_GPIO_PS_TO_PL0_DEVICE_ID);			// Lookup config
	XGpio_CfgInitialize(&PS_PL0, GPIO_PS_PL0_config,								//Init GPIO
			GPIO_PS_PL0_config->BaseAddress);
	XGpio_SetDataDirection(&PS_PL0, CH_PS_PL_0, 0xFFFFFFFF);					// Change PS to TL to outputpins
	XGpio_SetDataDirection(&PS_PL0, CH_PS_PL_1, 0xFFFFFFFF);					// Change PS to TL to outputpins

    /*
     * Setup for PS to PL 1
     */
	XGpio_Config *GPIO_PS_PL1_config;											// prepare config pointer
	GPIO_PS_PL1_config = XGpio_LookupConfig(XPAR_GPIO_PS_TO_PL1_DEVICE_ID);			// Lookup config
	XGpio_CfgInitialize(&PS_PL1, GPIO_PS_PL1_config,								//Init GPIO
			GPIO_PS_PL1_config->BaseAddress);
	XGpio_SetDataDirection(&PS_PL1, CH_PS_PL_0, 0xFFFFFFFF);					// Change PS to TL to outputpins
	XGpio_SetDataDirection(&PS_PL1, CH_PS_PL_1, 0xFFFFFFFF);					// Change PS to TL to outputpins


    /*
     * Setup for PL to PS
     */
	XGpio_Config *GPIO_PL_PS_config;											// prepare config pointer
	GPIO_PL_PS_config = XGpio_LookupConfig(XPAR_GPIO_PL_TO_PS0_DEVICE_ID);			// Lookup config
	XGpio_CfgInitialize(&PL_PS, GPIO_PL_PS_config,								//Init GPIO
			GPIO_PL_PS_config->BaseAddress);

	/*
	 * Setup Block RAM
	 */
	// config pointers for bram
	// channel A
	BRAM_Config_CH_A = XBram_LookupConfig(XPAR_BRAM_CH_A_DEVICE_ID);
	XBram_CfgInitialize(&BRAM_CH_A, BRAM_Config_CH_A, BRAM_Config_CH_A->CtrlBaseAddress);

	// channel B
	BRAM_Config_CH_B = XBram_LookupConfig(XPAR_BRAM_CH_B_DEVICE_ID);
	XBram_CfgInitialize(&BRAM_CH_B, BRAM_Config_CH_B, BRAM_Config_CH_B->CtrlBaseAddress);

	// FFT RE
	BRAM_Config_FFT_RE = XBram_LookupConfig(XPAR_BRAM_FFT_RE_DEVICE_ID);
	XBram_CfgInitialize(&BRAM_FFT_RE, BRAM_Config_FFT_RE, BRAM_Config_FFT_RE->CtrlBaseAddress);

	// FFT IM
	BRAM_Config_FFT_IM = XBram_LookupConfig(XPAR_BRAM_FFT_IM_DEVICE_ID);
	XBram_CfgInitialize(&BRAM_FFT_IM, BRAM_Config_FFT_IM, BRAM_Config_FFT_IM->CtrlBaseAddress);

}

void
cleanup_platform()
{
    disable_caches();
}
