/*
 * hw_bram.h
 *
 *  Created on: 19.06.2017
 *      Author: BioPerzept
 */

#ifndef SRC_HW_BRAM_H_
#define SRC_HW_BRAM_H_

float BRAM_readfloat( int MemBaseAddress, int RegisterOffset);
void BRAM_readfloat_array( int MemBaseAddress, float array[], int size);

void BRAM_writefloat( int MemBaseAddress, int RegisterOffset, float value);
void BRAM_writefloat_array( int MemBaseAddress, float values[], int size);
#endif /* SRC_HW_BRAM_H_ */
