/*
 * hw_fft.h
 *
 *  Created on: 13.06.2017
 *      Author: BioPerzept
 */

#ifndef SRC_HW_FFT_H_
#define SRC_HW_FFT_H_
#include "platform_config.h"

typedef
struct {
	uni_f_i re;
	uni_f_i im;
} hw_cmplx;

//int fwd_fft(float input[], float out_re[], float out_im[], int size);
int hwfft(float in_re[], float in_im[], float out_re[], float out_im[], int size, int dir);


#endif /* SRC_HW_FFT_H_ */
