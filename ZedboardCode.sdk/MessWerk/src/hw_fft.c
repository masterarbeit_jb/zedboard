/*
 * hw_fft.c
 *
 *  Created on: 13.06.2017
 *      Author: BioPerzept
 */

#include "hw_fft.h"
#include "hw_bram.h"

int hwfft(float in_re[], float in_im[], float out_re[], float out_im[], int size, int dir)
{

	for (int i = 0;i<size;i++)
	{
		BRAM_writefloat(BRAM_Config_FFT_RE->MemBaseAddress, i*4, in_re[i]);
		BRAM_writefloat(BRAM_Config_FFT_IM->MemBaseAddress, i*4, in_im[i]);
	}

	// FFT Konfigurieren
	PS_PL_1.s.FFT_FWD = dir;
	PS_PL_1.s.FFT_SCALE = 0x6AB;
	PS_PL_1.s.FFT_unused  = 0;
	XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_1, PS_PL_1.asInt);
	PS_PL_0.s.fft_start = 1;

	XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
	PS_PL_0.s.fft_start = 0;
	XGpio_DiscreteWrite(&PS_PL0, CH_PS_PL_0, PS_PL_0.asInt);
	// Warten, bis sie fertig ist.
	do {
		PL_PS_0.asInt = XGpio_DiscreteRead(&PL_PS, CH_PL_PS_0);
	}while(PL_PS_0.s.FFT_ready == 0);

	BRAM_readfloat_array(BRAM_Config_FFT_RE->MemBaseAddress, out_re, size);
	BRAM_readfloat_array(BRAM_Config_FFT_IM->MemBaseAddress, out_im, size);

	double size_kehrwert = 1.0/size;

	// Skalieren
	for(int i = 0; i <size-1; i++)
	{
		//out_re[size-i] = out_re[size-(i+1)];
		//out_im[size-i] = out_im[size-(i+1)];
		//out_re[i] = out_re[i] * size_kehrwert;
		//out_im[i] = out_im[i] * size_kehrwert;
	}
	//out_im[0] = 0;
	//out_re[0] = 0;
	return 0;
}
