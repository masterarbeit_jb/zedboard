/*
 * messurement.h
 *
 *  Created on: 05.07.2017
 *      Author: BioPerzept
 */

#ifndef SRC_MESSUREMENT_H_
#define SRC_MESSUREMENT_H_
#include "platform_config.h"

void new_messurement(chan_data *CH_A, chan_data *CH_B, int size);

#endif /* SRC_MESSUREMENT_H_ */
