/*
 * windows.h
 *
 *  Created on: 05.07.2017
 *      Author: BioPerzept
 */

#ifndef SRC_WINDOWS_H_
#define SRC_WINDOWS_H_

extern float flattopwin[2048];
void windowing(float data_in[], float data_out[], float window[], int size);

#endif /* SRC_WINDOWS_H_ */
