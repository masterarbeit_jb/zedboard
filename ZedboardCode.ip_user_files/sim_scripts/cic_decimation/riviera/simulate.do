onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+cic_decimation -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -L xil_defaultlib -L xpm -L xbip_utils_v3_0_7 -L axi_utils_v2_0_3 -L cic_compiler_v4_0_11 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.cic_decimation xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {cic_decimation.udo}

run -all

endsim

quit -force
