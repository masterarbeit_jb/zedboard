onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -lib xil_defaultlib floating_point_0_opt

do {wave.do}

view wave
view structure
view signals

do {floating_point_0.udo}

run -all

quit -force
