onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -lib xil_defaultlib block_ram_2048_opt

do {wave.do}

view wave
view structure
view signals

do {block_ram_2048.udo}

run -all

quit -force
