onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+DDR2SDR -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -L xil_defaultlib -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.DDR2SDR xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {DDR2SDR.udo}

run -all

endsim

quit -force
