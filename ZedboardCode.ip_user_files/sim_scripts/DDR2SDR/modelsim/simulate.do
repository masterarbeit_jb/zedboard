onbreak {quit -f}
onerror {quit -f}

vsim -voptargs="+acc" -t 1ps -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -L xil_defaultlib -L xpm -L unisims_ver -L unimacro_ver -L secureip -lib xil_defaultlib xil_defaultlib.DDR2SDR xil_defaultlib.glbl

do {wave.do}

view wave
view structure
view signals

do {DDR2SDR.udo}

run -all

quit -force
