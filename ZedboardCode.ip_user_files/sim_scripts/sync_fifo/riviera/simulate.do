onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+sync_fifo -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -L xil_defaultlib -L xpm -L fifo_generator_v13_1_2 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.sync_fifo xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {sync_fifo.udo}

run -all

endsim

quit -force
