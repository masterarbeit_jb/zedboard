onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -lib xil_defaultlib sync_fifo_opt

do {wave.do}

view wave
view structure
view signals

do {sync_fifo.udo}

run -all

quit -force
