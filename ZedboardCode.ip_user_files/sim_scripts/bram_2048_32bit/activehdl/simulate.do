onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+bram_2048_32bit -pli "D:/Xilinx/Vivado/2016.3/lib/win64.o/libxil_vsim.dll" -L xil_defaultlib -L xpm -L blk_mem_gen_v8_3_4 -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.bram_2048_32bit xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {bram_2048_32bit.udo}

run -all

endsim

quit -force
